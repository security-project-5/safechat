# README #


Welcome to SafeChat!

To get started:
Create and manage your account by going to: https://192.168.5.64/
on your favorite web browser!

Then, download the SafeChat client by clicking Downloads on this bitbucket repositiory and download the ENTIRE repository. Downloading just the JAR file is not sufficient.

After creating an account, launch SafeChat from the command line by doing:
java -jar safechat2.0.jar
NOTICE: You must have Java 8 installed to use SafeChat!

All of SafeChat's source code is available to you in this repository under the safechat_source folder.

Also feel free to check out our protocol documentation in the Protocol.pdf file!