#!flask/Scripts/python       
from wtforms import Form
from wtforms.csrf.session import SessionCSRF
from datetime import timedelta
from flask import Flask, session

class BaseForm(Form):
  class Meta:
    csrf = True
    csrf_class = SessionCSRF
    csrf_secret = b'EP900d71fj4Gx3Sj8kjL3wgdaVdfcdW9fvCZeHYm'
    csrf_time_limit = timedelta(minutes=20)
    
    @property
    def csrf_context(self):
      return session
    
    def validate_on_submit():
      return True