#!flask/Scripts/python
import pymysql
import logging
from model import User
from random import randint

db = pymysql.connect(host="sql3.freesqldatabase.com", user="sql3107916", passwd="SF8cXr51Xz", db="sql3107916")
db.autocommit(True)
  
def userExists(email):
  cur = db.cursor()
  cmd = "select count(*) from user where email=%s"
  cur.execute(cmd, (email,))
  results = cur.fetchone()
  result = True if results[0] == 1 else False
  logging.warn("cur.fetchone(): "+str(results[0]))
  logging.warn("rowcount: "+str(cur.rowcount))
  cur.close() 
  return result 
  
def userForEmail(email):
  cur = db.cursor()
  cmd = "select user_id, email, firstname, lastname from user where email=%s"
  cur.execute(cmd, (email,))   
  result = ''
  if cur.rowcount == 0:
    result = None
  else:
    results = cur.fetchone()
    Use = {'id' : results[0], 'firstname' : results[1], 'lastname' : results[2]}
    id = results[0]
    email = results[1]
    firstname =  results[2]
    lastname = results[3]
  return User(id, email, firstname, lastname)
  
def firstnameForEmail(email):
  fullnameDict = fullnameDictForEmail(email)
  if not fullnameDict:
    return None
  return fullnameDict['firstname']  
  
def fetchoneclose(cursor):
  if cursor.rowcount == 0:
    result = None
  else:
    result = cursor.fetchone()[0]
  return result

def sessionCookieFor(email):
  cur = db.cursor()  
  cmd = "select cookie from session_cookie inner join user on(session_cookie.email = user.email) where user.email = %s"  
  cur.execute(cmd, (email, ))
  return fetchoneclose(cur)
  
def deleteSessionCookie(email):
  cur = db.cursor()  
  cmd = "delete from session_cookie where email=%s"  
  cur.execute(cmd, (email,))      
  
def saveSessionCookie(cookie, email):
  #delete old
  deleteSessionCookie(email)
  cur = db.cursor()  
  cmd = "insert into session_cookie (email, cookie) values (%s, %s)"  
  cur.execute(cmd, (email, cookie))
  cur.close()

def getPassword(email):
  cur = db.cursor()  
  cmd = "select password from user where email = %s"
  cur.execute(cmd, (email,))
  return fetchoneclose(cur);
  
  #for row in cur.fetchall():
  #  t+=str(row[0])+", "+row[1]
  #db.close()
  
def insertUser(email, firstname, lastname, phone, password):
  cur = db.cursor()
  cmd = "insert into user (id, email, firstname, lastname, phone, password) values (%s, %s, %s, %s, %s)"
  cur.execute(cmd, (randint(0, 99999999999999), email, firstname, lastname, phone, password))
  cur.close()    


