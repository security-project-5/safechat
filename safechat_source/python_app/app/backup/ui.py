#!flask/Scripts/python
from flask import render_template
from prefill import emailInput
from prefill import pwInput

def login(email='', emailErr='', pwErr=''):
  return render_template('login.html', emailErr=emailErr, pwErr=pwErr, 
  emailInput = emailInput(email), pwInput = pwInput(email is not None and len(email) > 0))
  
  

