#!flask/Scripts/python
from wtforms import Form, BooleanField, TextField, PasswordField, validators
from passlib.hash import pbkdf2_sha256
import validate
from login import userExists
from flask import render_template
from prefill import formInput
from prefill import emailInput
from security import hash
import email
import re
import db

class RegistrationForm(Form):
    username = TextField('Username', [validators.Length(min=4, max=25)])
    email = TextField('Email', [validators.Length(min=6, max=35)])
    password = PasswordField('Password', [
        validators.Required(),
        validators.EqualTo('confirm', message='Passwords do not match')
    ])
    confirm = PasswordField('Confirm password')
    accept_tos = BooleanField('I agree with the terms and conditions', [validators.Required()])


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User(form.username.data, form.email.data,
                    form.password.data)
        hash = hash(form.password.data)
        db.insertUser(user, hash)
        flash('Thank you for registering')
        return redirect(url_for('login'))
    return render_template('register.html', form=form)

def register(email, password):
  hash = hash(password)
  db.insertUser(email, hash)
  
def handle(email, firstname, lastname, phone, password, confirmPassword):
  emailErr, firstnameErr, lastnameErr, phoneErr, emailErr, confPwErr = '','','','','',''
  if not email or not validate.email(email):
    emailErr = "Invalid e-mail"
  if email and len(email) > 40:
    emailErr = "Email is too long. Get a reasonable one"
  if not firstname or len(firstname) < 2:
    firstnameErr = "First name should have at least 1 character"
  if not lastname or len(lastname) < 2:
    lastnameErr = "Last name should have at least 1 character"  
  if userExists(email): 
    emailErr = "User with this e-mail already exists"
  if phone and not validate.phone(phone):
    phoneErr = "Invalid phone number"
  if not password:
    pwErrs = {'1', "Enter password"}
  elif not confirmPassword:
    confPwErr = "Enter confirm password"  
  else: 
    pwErrs = validate.pw(password)
    if password != confirmPassword: 
      confPwErr = "Passwords do not match"
      
  #If everything OK, insert new user to db
  if not (emailErr or firstnameErr or lastnameErr or phoneErr or pwErrs or confPwErr):
    db.insertUser(email, firstname, lastname, phone, encrypt(password))
    #email.send('info@safechat.com', email, '')
    return render_template('login.html', barMsg="Check your inbox for a confirmation link to complete your registration", emailInput = emailInput(email)) 

    
  #Otherwise go back with errors and prefilled values 
  return render_template('register.html', 
      emailErr = emailErr, 
      firstnameErr = firstnameErr, 
      lastnameErr = lastnameErr, 
      phoneErr = phoneErr,
      pwErrs = pwErrs, 
      confPwErr = confPwErr,
      #Prefill inputs
      emailInput = formInput('email', 'email', 'form-control', 'E-mail', email, 'required autofocus'),
      firstnameInput = formInput('text', 'firstname', 'form-control', 'First name', firstname, 'required'), 
      lastnameInput = formInput('text', 'lastname', 'form-control', 'Last name', lastname, 'required'),
      phoneInput = formInput('tel', 'phone', 'form-control', 'Phone number', phone, ''))
 


