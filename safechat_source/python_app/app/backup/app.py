#!flask/Scripts/python
import os
import os.path
import login
import register
from flask import render_template
from flask import request
import pymysql
from flask import Flask
import ctypes
import db
import ui
from prefill import formInput
from prefill import emailInput
from prefill import pwInput
import logging

#app = Flask(__name__, static_url_path='/static', static_folder='static_test')
app = Flask(__name__)
app.config.from_object('config')
app.debug = True #DELETE BEFORE DEPLOYMENT
if __name__ == '__main__':
  app.run()
login.init(app)
  
@app.route('/', methods=['GET'])
def loginPage():
  email = 'dforev@gmail.com'#request.cookies.get('email')
  firstname = request.cookies.get('firstname')
  sessionCookie = request.cookies.get('sessionCookie')
  if login.check(sessionCookie, email):
    return render_template('dashboard.html', firstname = firstname)
  return ui.login(email=email)
  
@app.route('/', methods=['POST'])
def handleLogin():
  #TODO num attempts check
  return login.handle(request.form['email'], request.form['password'])
  
@app.route('/dashboard', methods=['GET'])
def dashboard():
  #TODO check sessionid in cookie
  s=request.cookies.get('session')
  logging.warn("actual: "+str(s))
  firstname = request.cookies.get('firstname')
  email = request.cookies.get('email')
  if login.check(s, email):
    logging.warn("MATCH")
  else: 
    logging.warn("NO MATCH") 
  email = request.cookies.get('email')
  return render_template('dashboard.html', firstname = firstname if firstname else None)  
  
'''@app.route('/register', methods=['GET'])
def registerPage():
  return render_template('register.html',
      emailInput = emailInput(''),
      firstnameInput = formInput('text', 'firstname', 'form-control', 'First name', '', 'required'), 
      lastnameInput = formInput('text', 'lastname', 'form-control', 'Last name', '', 'required'),
      phoneInput = formInput('tel', 'phone', 'form-control', 'Phone number', '', ''))
'''
  
@app.route('/register', methods=['POST'])
def handleRegister():
  return register.handle(
      request.form['email'], 
      request.form['firstname'], 
      request.form['lastname'], 
      request.form['phone'], 
      request.form['password'],
      request.form['confirm-password'])




