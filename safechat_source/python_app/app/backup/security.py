#!flask/Scripts/python
from passlib.hash import pbkdf2_sha256
import re
import pyaes
import logging
import os  
import six                       
import db     

def hash(password):
  hash = pbkdf2_sha256.encrypt(password, rounds=50000, salt_size=16)
  return hash
  
def hashFast(password):
  hash = pbkdf2_sha256.encrypt(password, rounds=5000, salt_size=8)
  return hash  

def verifyPw(password, hash):  
  return pbkdf2_sha256.verify(password, hash)

def sessionCookie(email, id, pwHash):
  #c = encrypt('1', '1340134013401340')
  #logging.warn("c: "+str(c))
  #p = decrypt(c, '1340134013401340')
  #logging.warn("p: "+str(p))
  #logging.warn(email+", "+str(id)+", " + pwHash +", "+hashFast('1340134013401340'))
  cookie = encrypt(email + str(id) + pwHash, encrypt(pwHash, hashFast('1340134013401340')))[:100]
  return cookie
  
def verifySessionCookie(cookie, pwHash, email):
  if cookie is None or pwHash is None or email is None:
    return False
  expected = db.sessionCookieFor(email)
  logging.warn("expected: "+expected)
  return cookie is not None and expected == cookie 
  
def encrypt(text, key):
  if not isinstance(key, six.binary_type):
    key = bytes(key, 'cp1252')
  key = key[:32]
  counter = pyaes.Counter(initial_value = 134)
  aes = pyaes.AESModeOfOperationCTR(key, counter = counter)
  return aes.encrypt(text.encode('cp1252', 'replace'))
  #cipher = AES.new(key, AES.MODE_EAX)
  #ciphertext, tag = cipher.encrypt_and_digest(data)
  #return ciphertext, tag, cipher.nonce
  
def decrypt(text, key):
  key = bytes(key, 'cp1252')[:32]
  counter = pyaes.Counter(initial_value = 134)
  aes = pyaes.AESModeOfOperationCTR(key, counter = counter)
  return aes.decrypt(text).decode('cp1252', 'replace')
  #cipher = AES.new(key, AES.MODE_EAX)
  #plaintext = cipher.decrypt_and_verify(ciphertext, tag)
  #return plaintext