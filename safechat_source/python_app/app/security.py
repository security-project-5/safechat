#!flask/Scripts/python
from passlib.hash import pbkdf2_sha256
from hashlib import sha256
import base64
import re
import pyaes
import time
import logging
import os  
import six                       
import db     
import string
from flask import url_for
import random
from io import BytesIO
from captcha.image import ImageCaptcha

def hash(password, username):
  #hash = pbkdf2_sha256.encrypt(password, rounds=50000, salt_size=16)
  input=encode(password+username)
  hash = base64.b64encode(sha256(input).digest())
  return hash
  
def verifyPw(password, username, hash):  
  input = encode(password+username)
  return base64.b64encode(sha256(input).digest()).decode(encoding="US-ASCII") == hash
  #return pbkdf2_sha256.verify(password, hash)

def encode(input):
  return input.encode('US-ASCII')

def sessionCookie(email, id, pwHash):
  #c = encrypt('1', '1340134013401340')
  #logging.warn("c: "+str(c))
  #p = decrypt(c, '1340134013401340')
  #logging.warn("p: "+str(p))
  #logging.warn(email+", "+str(id)+", " + pwHash +", "+hashFast('1340134013401340'))
  cookie = encrypt(email + str(id) + pwHash, encrypt(pwHash, hashFast('1340134013401340')))[:100]
  return cookie
  
def verifySessionCookie(cookie, pwHash, email):
  if cookie is None or pwHash is None or email is None:
    return False
  expected = db.sessionCookieFor(email)
  return cookie is not None and expected == cookie 
  
def encrypt(text, key):
  if not isinstance(key, six.binary_type):
    key = bytes(key, 'cp1252')
  key = key[:32]
  counter = pyaes.Counter(initial_value = 134)
  aes = pyaes.AESModeOfOperationCTR(key, counter = counter)
  return aes.encrypt(text.encode('cp1252', 'replace'))
  #cipher = AES.new(key, AES.MODE_EAX)
  #ciphertext, tag = cipher.encrypt_and_digest(data)
  #return ciphertext, tag, cipher.nonce
  
def decrypt(text, key):
  key = bytes(key, 'cp1252')[:32]
  counter = pyaes.Counter(initial_value = 134)
  aes = pyaes.AESModeOfOperationCTR(key, counter = counter)
  return aes.decrypt(text).decode('cp1252', 'replace')
  #cipher = AES.new(key, AES.MODE_EAX)
  #plaintext = cipher.decrypt_and_verify(ciphertext, tag)
  #return plaintext
  
def rnd_string(size=6, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
  return ''.join(random.choice(chars) for _ in range(size))

root = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER_CAPTCHA = os.path.join(root, 'static', 'img', 'captcha')

def os_path_for_captcha(captcha):
  return os.path.join(UPLOAD_FOLDER_CAPTCHA, captcha+'.png')
  
def delete_captcha(captcha):
  fpath = os_path_for_captcha(captcha)
  if os.path.exists(fpath):
    os.remove(fpath)

def delete_old_captchas():
  for f in os.listdir(UPLOAD_FOLDER_CAPTCHA):
    now = time.time()
    fpath = os.path.join(UPLOAD_FOLDER_CAPTCHA, f)
    mtime = os.stat(fpath).st_mtime
    if mtime < now - 600: #Delete if older than 10 min
      if os.path.exists(fpath):
        os.remove(fpath)
      

def captcha():
  image = ImageCaptcha()
  text = rnd_string(size=6)
  data = image.generate(text)
  assert isinstance(data, BytesIO)
  
  #For windows
  #root = root.replace('\\', '/')
  webpath = url_for('static',filename=os.path.join('img', 'captcha', text)+'.png'
)
  ospath = os_path_for_captcha(text)
  image.write(text, ospath)
  return text, webpath, ospath
