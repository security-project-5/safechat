#!flask/Scripts/python
from flask import render_template
from flask import Flask, request, redirect, url_for
from werkzeug import secure_filename
from baseform import BaseForm
import os                                                         
import logging
import ui
import util
import db
import time

ALLOWED_EXTENSIONS_IMAGE = set(['png', 'jpg', 'jpeg', 'gif'])
root = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER_PROFILE_PHOTO = root+'/static/img/profile/'
#UPLOAD_FOLDER_PROFILE_PHOTO = root+'/static/img/profile/'

def allowed_image(filename):
    return '.' in filename and \
           extension(filename) in ALLOWED_EXTENSIONS_IMAGE
           
def extension(filename):
    return filename.rsplit('.', 1)[1]  
    
def get_profile_photo_for(user_id):
    filename = db.profileImageNameForUserId(user_id)
    if not filename:
      return ''
    #Find photo by extension
    for ext in ALLOWED_EXTENSIONS_IMAGE:
      sys_path = os.path.join(UPLOAD_FOLDER_PROFILE_PHOTO, filename+'.'+ext)
      if os.path.isfile(sys_path):
        web_path = url_for('static',filename='img/profile/'+filename+'.'+ext)
        return web_path
    return None
    
class UploadImageForm(BaseForm):
    def validate_on_submit():
      return True

def upload_profile_photo(file, user_id):
    error = ''
    filename = secure_filename(file.filename)
    filename = filename.lower()
    
    #Check file type               
    if not file or not allowed_image(filename):
      error = "Invalid file type. Supported image formats are .png, .jpg, .gif"
      return None, error  
    
    #Delete existing photos!
    #1) from DB
    oldProfilePhoto = db.profileImageNameForUserId(user_id)
    db.deleteOldProfilePhotos(user_id)
    #2) from disk
    if oldProfilePhoto:
      for ext in ALLOWED_EXTENSIONS_IMAGE:
        sys_path = os.path.join(UPLOAD_FOLDER_PROFILE_PHOTO, oldProfilePhoto+'.'+ext)
        if os.path.isfile(sys_path):
          os.remove(sys_path)
         
    #Save file
    timestamp = time.time() #We append timestamp to file to prevent browser cac$
    path = os.path.join(UPLOAD_FOLDER_PROFILE_PHOTO, str(user_id)+'_'+str(timestamp)+"."+extension(filename))
    file.save(path)
    filesize = os.stat(path).st_size
    if filesize > 1000000: #If bigger than 1 MB, reject and delete
      error = 'Error: Maximum allowed photo size is 1 MB'
      os.remove(path)
      path = None
    if not error:
      #Save to DB
      db.insertProfilePhoto(user_id, timestamp)
    return path, error
