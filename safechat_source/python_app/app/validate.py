#!flask/Scripts/python
from passlib.hash import pbkdf2_sha256
import re
import db

EMAIL_REGEX = r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$"
  
def check_pw(password):
  lengthErr = len(password) < 8 or len(password) > 40
  noDigitErr = re.search(r"\d", password) is None
  noLowercaseErr = re.search(r"[a-z]", password) is None 
  noUppercaseErr = re.search(r"[A-Z]", password) is None
  if not (lengthErr or noDigitErr or noLowercaseErr or noUppercaseErr):
    return {} #Important
  return {
    'lengthErr' : 'must have at least 8 letters' if lengthErr else '',
    'noDigitErr' : 'must contain at least one digit' if noDigitErr else '',
    'noLowercaseErr' : 'must contain a lower case letter' if noLowercaseErr else '',
    'noUppercaseErr' : 'must contain an upper case letter' if noUppercaseErr else ''}
    
def check_phone(password):
  return re.match(r"\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4}", password)
  
def check_email(email):
  return re.match(EMAIL_REGEX, email)
  
def check_user(email, firstname, lastname, phone, password = None, confirmPassword = None):
  emailErr, firstnameErr, lastnameErr, phoneErr, emailErr, confPwErr = '','','','','',''
  pwErrs = {}
  if not email or not check_email(email):
    emailErr = "Invalid e-mail"
  if email and len(email) > 40:
    emailErr = "Email is too long. Get a reasonable one"
  if not firstname or len(firstname) < 1:
    firstnameErr = "Enter first name"
  if firstname and len(firstname) > 20:
    firstnameErr = "First name is too long, maximum 20 characters allowed"
  if not lastname or len(lastname) < 1:
    lastnameErr = "Enter last name"  
  if lastname and len(lastname) > 20:
    lastnameErr = "Last name is too long, maximum 20 characters allowed"  
  if db.userExists(email): 
    emailErr = "User with this e-mail already exists"
  if phone and (not check_phone(phone) or len(phone) < 9 or len(phone) > 13):
    phoneErr = "Invalid phone number"
  if not password:
    pwErrs = {'1' : "Enter password"}
  elif not confirmPassword:
    confPwErr = "Enter confirm password"  
  else: 
    pwErrs = check_pw(password)
    if password != confirmPassword: 
      confPwErr = "Passwords do not match"
  return {
  'emailErr' : emailErr,
  'firstnameErr' : firstnameErr,
  'lastnameErr' : lastnameErr,
  'phoneErr' : phoneErr,
  'emailErr' : emailErr,
  'confPwErr' : confPwErr,
  'pwErrs' : pwErrs
  }
  
  
def isOk(errors):
   emailErr = errors['emailErr'] 
   firstnameErr = errors['firstnameErr'] 
   lastnameErr = errors['lastnameErr'] 
   phoneErr = errors['phoneErr']
   pwErrs = errors['pwErrs'] 
   confPwErr = errors['confPwErr'] 
   return not (emailErr or firstnameErr or lastnameErr or phoneErr or (len(pwErrs) > 0) or confPwErr)
  
