#!flask/Scripts/python  
from baseform import BaseForm     
from wtforms import Form, HiddenField, BooleanField, StringField, PasswordField, validators
from flask.ext.login import LoginManager
from passlib.hash import pbkdf2_sha256
import db
import security
import sys
import ui
import logging
from flask import render_template, session
from flask import make_response
from prefill import emailInput

lm = LoginManager()

class LoginForm(BaseForm):
    email = StringField('E-mail')#, [validators.Length(min=2, max=25)])
    password = PasswordField('Password')
    remember_me = BooleanField('Remember me')
    captcha = StringField('Enter captcha')
    captcha_expected = HiddenField('Captcha')
    
    def validate_on_submit():
      return True

def init(app):
  lm.init_app(app)
  
@lm.user_loader
def load_user(user_id):
  return db.userForId(user_id)

def userExists(email):
  return db.userExists(email)

def pwMatches(email, password):
  hash = db.getPasswordForEmail(email)
  logging.warn("hash: "+hash)
  if hash == None:
    return False
  result = security.verifyPw(password, email, hash)
  return result
  
def incr_attempts(client_ip, email=None):
  return db.incr_login_attempts(client_ip, email)

def get_attempts(client_ip, email=None):
  return db.get_login_attempts(client_ip, email)  
  
def delete_attempts(email, client_ip):
  return db.delete_login_attempts(email, client_ip)    
  

