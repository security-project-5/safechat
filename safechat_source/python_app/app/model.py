#!flask/Scripts/python
import db

class User:
    def __init__(self, user_id, email, firstname, lastname, phone, active=True):
        self.user_id = user_id
        self.email = email
        self.firstname = firstname
        self.lastname = lastname
        self.phone = phone
        self.active = active
        
    def get_id(self):
        return self.user_id

    def is_active(self):
        return self.active

    def is_anonymous(self):
        return False

    def is_authenticated(self):
        return True