#!flask/Scripts/python       
from wtforms import BooleanField, TextField, StringField, PasswordField, FileField, validators
from flask.ext.wtf import Form
from baseform import BaseForm
import validate
                                                            
class EditForm(BaseForm):
    email = TextField('')#, [validators.Length(min=6, max=40), validators.Regexp(validate.EMAIL_REGEX, message="Enter a valid e-mail")])
    firstname = TextField('')#, [validators.Length(min=2, max=40)])
    lastname = TextField('')#, [validators.Length(min=2, max=40)])
    phone = TextField('')#, [validators.Length(min=0, max=13)])
    profilePhoto = FileField('Profile photo')
    oldPassword = PasswordField('Old password')
    newPassword = PasswordField('New password')
           
    def validate_on_submit():
      return True
      
def setupProfileForm(form, email, firstname, lastname, phone):
  form.email.default = email
  form.firstname.default = firstname
  form.lastname.default = lastname
  form.phone.default = phone
  form.process()
