#!flask/Scripts/python

from PIL import Image
from resizeimage import resizeimage

def resize_image(path):
  fd_img = open(path, 'r', encoding="utf8")
  img = Image.open(fd_img)
  img = resizeimage.resize_width(img, 200)
  img.save(path, img.format)
  fd_img.close()