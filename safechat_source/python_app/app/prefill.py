#!flask/Scripts/python
from html import escape

def formInput(inputtype, name, _class, placeholder, value, extra):
  return '<input type="' + inputtype + '" name="' + name + '" class="' + _class + '" placeholder="' + placeholder + '" value="' + escape(value, quote=True) + '" ' + extra + '>'
  
def emailInput(email):
  return formInput('email', 'email', 'form-control', 'E-mail', email if email else '', 'required' + (' autofocus' if email else ''))
def pwInput(autofocus):
  return formInput('password', 'password', 'form-control', 'Password', '', 'required' + (' autofocus' if autofocus == True else ''))