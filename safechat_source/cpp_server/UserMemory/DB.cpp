/*
 *
 *  Created on: Mar 21, 2016
 *      Author: jan
 */

#include "../PublicKey.h"
#include "DB.h"
#include "MySQLConnector.h"
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

DB::DB() {
	MySQLConnector *c = new MySQLConnector();
	this->con = c->connect();
	cout<<"done initializing database"<<endl;
}

DB::~DB() {
	this->con->close();
}

bool DB::verifyLogin(string username, string saltedHashOfPassword) {
	sql::PreparedStatement *pstmt;
	pstmt = con->prepareStatement("select password from user where email=?");
	pstmt->setString(1, username);
	pstmt->executeUpdate();
	sql::ResultSet *res = pstmt->executeQuery();
	delete pstmt;
	if (res->next()) {
	   return res->getString("password") == saltedHashOfPassword;
	}
	return false;
}

unsigned long long DB::userIdFor(string username) {
	sql::PreparedStatement *pstmt;
	pstmt = con->prepareStatement("select user_id from user where email=?");
	pstmt->setString(1, username);
	pstmt->executeUpdate();
	sql::ResultSet *res = pstmt->executeQuery();
	delete pstmt;
	if (res->next()) {
		return res->getUInt64("user_id");;
	}
	return ULLONG_MAX;
}

string DB::usernameFor(unsigned long long user_id) {
	sql::PreparedStatement *pstmt;
	pstmt = con->prepareStatement("select email from user where user_id=?");
	pstmt->setUInt64(1, user_id);
	pstmt->executeUpdate();
	sql::ResultSet *res = pstmt->executeQuery();
	delete pstmt;
	if (res->next()) {
		return res->getString("email");;
	}
	return "";
}


void DB::addPublicKey(string username, string device_id, PublicKey pk)  {
	sql::PreparedStatement *pstmt;
	unsigned long long user_id = userIdFor(username);
	if(user_id == ULLONG_MAX) {
		//Error - No user with this username found
		throw PilgrimException("No user with username "+username+" found!");
	}
	cout << "ADD PUBLIC KEY" << endl;
	try {
		pstmt = con->prepareStatement("INSERT INTO user_device(user_id, device_id, public_key) values (?, ?, ?)");
		pstmt->setUInt64(1, user_id);
		pstmt->setString(2, device_id);
		pstmt->setString(3, pk._64_byte_array);
		pstmt->executeUpdate();
		pstmt->executeQuery();
	}
	//TODO For some reason, DB tries to insert twice and throws duplicate entry excpetion: ignoring for now.
	catch(std::exception& e){
		std::cerr<< "Ignoring DB exception: " << e.what() << std::endl;
		return;
	}
	delete pstmt;
}

PublicKey DB::getPublicKey(string device_id)  throw(PilgrimException) {
	sql::PreparedStatement *pstmt;

	pstmt = con->prepareStatement("select public_key from user_device where device_id=?");

	pstmt->setString(1, device_id);
	pstmt->executeUpdate();
	sql::ResultSet *res = pstmt->executeQuery();
	delete pstmt;
	if (res->next()) {
	   return PublicKey(res->getString("public_key"));
	}
	return PublicKey("");
}

UserInfo DB::getUserAndDeviceInfo(string username) throw(PilgrimException) {
	sql::PreparedStatement *pstmt, *pstmt2;
	unsigned long long user_id = userIdFor(username);
	if(user_id == ULLONG_MAX) {
		//Error - No user with this username found
		throw PilgrimException("No user with username "+username+" found!");
	}
	std::string phone_number, email, firstname, lastname;
	std::vector<DeviceInfo> devices;
	//Fetch phone number
	pstmt = con->prepareStatement("select phone, firstname, lastname from user where user_id=?");
	pstmt->setUInt64(1, user_id);
	pstmt->executeUpdate();
	sql::ResultSet *res = pstmt->executeQuery();
	delete pstmt;
	if (res->next()) {
		phone_number = res->getString("phone");
		firstname = res->getString("firstname");
		lastname = res->getString("lastname");
	}
	//Fetch devices
	pstmt2 = con->prepareStatement("select device_id, public_key from user_device where user_id=?");
	pstmt2->setUInt64(1, user_id);
	pstmt2->executeUpdate();
	sql::ResultSet *res2 = pstmt2->executeQuery();
	delete pstmt2;
	while (res2->next()) {
		DeviceInfo di = DeviceInfo(PublicKey(res2->getString("public_key")), res2->getString("device_id"));
		devices.push_back(di);
	}
	UserInfo result(username, firstname, lastname, username, phone_number);

	for(size_t i=0;i<devices.size();i++){
		result.addDevice(devices[i]);
	}
	return result;
}

void DB::storeSessionInit(string from_user, string from_device, string to_device, string session_id, string msg)  {
	sql::PreparedStatement *pstmt;
	unsigned long long from_id = userIdFor(from_user);
	if(from_id == ULLONG_MAX) {
		//Error - No user with this username found
		throw PilgrimException("No user with username "+from_user+" found!");
	}
	try {
		pstmt = con->prepareStatement("INSERT INTO session(session_id, from_id, from_device, to_device, msg) "
				"values (?, ?, ?, ?, ?)");
		pstmt->setString(1, session_id);
		pstmt->setUInt64(2, from_id);
		pstmt->setString(3, from_device);
		pstmt->setString(4, to_device);
		pstmt->setString(5, msg);
		pstmt->executeUpdate();
		pstmt->executeQuery();
	}
	//TODO For some reason, DB tries to insert twice and throws duplicate entry exception: ignoring for now.
	catch(std::exception& e){
		std::cerr<< "Ignoring DB exception: " << e.what() <<std::endl;
		return;
	}
	delete pstmt;
}

void DB::storeMessage(unsigned int message_id, string from_username, string from_device, string to_username, string group_name, string session_id, string msg)  {
	unsigned long long user_id_from = userIdFor(from_username);
	if(user_id_from == ULLONG_MAX) {
		//Error - No user with this username found
		throw PilgrimException("No user with username "+from_username+" found!");
	}
	unsigned long long user_id_to = userIdFor(to_username);
	if(user_id_to == ULLONG_MAX) {
		//Error - No user with this username found
		throw PilgrimException("No user with username "+to_username+" found!");
	}
	sql::PreparedStatement *pstmt;
	try {
		pstmt = con->prepareStatement("INSERT INTO message(message_id, from_id, from_device, to_id, group_name, session_id, text) "
				"values (?, ?, ?, ?, ?, ?, ?)");
		pstmt->setUInt(1, message_id);
		pstmt->setUInt64(2, user_id_from);
		pstmt->setString(3, from_device);
		pstmt->setUInt64(4, user_id_to);
		pstmt->setString(5, group_name);
		pstmt->setString(6, session_id);
		pstmt->setString(7, msg);
		pstmt->executeUpdate();
		pstmt->executeQuery();
		//TODO For some reason, DB tries to insert twice and throws duplicate entry exception: ignoring for now.
	} catch(std::exception& e) {
		std::cerr<< "Ignoring DB exception: " << e.what() <<std::endl;
		return;
	}
	delete pstmt;
}

SessionList DB::getSessionsWithMsgsFor(string username, string device_id)  throw(PilgrimException) {
	cout << "getSessionsWithMsgsFor" << endl;
	SessionList sl;
	//Fetch sessions
	sql::PreparedStatement *pstmt;
	pstmt = con->prepareStatement("select session_id, from_id, from_device, msg from session where to_device=? AND ( select count(*) from session_delivery where session_delivery.session_id=session.session_id AND to_device=?) = 0");
	pstmt->setString(1, device_id);
	pstmt->setString(2, device_id);
	pstmt->executeUpdate();
	sql::ResultSet *res = pstmt->executeQuery();
	delete pstmt;
	while (res->next()) {
		string sid = res->getString("session_id");
		cout << "got session id: " << sid << endl;
		unsigned long long from_id = res->getUInt64("from_id");
		string from_user = usernameFor(from_id);
		if(from_user == "") {
			//Error - No user with this user_id found
			throw PilgrimException("No user with user_id found!");
		}
		string from_dev = res->getString("from_device");
		string msg = res->getString("msg");
		Session s(sid, from_user, from_dev, msg);
		sl.addSession(s);
		sql::PreparedStatement *storeSessionDeliveryStatement;
		try {
			cout << "storing: " << sid << ", " << device_id << " into session_delivery" << endl;
			storeSessionDeliveryStatement = con->prepareStatement("INSERT INTO session_delivery(session_id, to_device) values (?, ?)");
			storeSessionDeliveryStatement->setString(1, sid);
			storeSessionDeliveryStatement->setString(2, device_id);
			storeSessionDeliveryStatement->executeUpdate();
			storeSessionDeliveryStatement->executeQuery();
			//TODO For some reason, DB tries to insert twice and throws duplicate entry exception: ignoring for now.
		} catch(std::exception& e) {
			std::cerr<< "Ignoring DB exception: " << e.what() <<std::endl;
		}
		delete storeSessionDeliveryStatement;
	}
	//Fetch messages
	unsigned long long user_id = userIdFor(username);
	if(user_id == ULLONG_MAX) {
		//Error - No user with this username found
		throw PilgrimException("No user with username "+username+" found!");
	}
	sql::PreparedStatement *pstmt2;
	cout << "getting messages for: " << username << endl;
	pstmt2 = con->prepareStatement("select message_id, session_id, from_id, from_device, group_name, text from message join user on (message.to_id = user.user_id) where to_id=? AND ( select count(*) from message_delivery where message_delivery.message_id=message.message_id AND to_device=? ) = 0");
	pstmt2->setUInt64(1, user_id);
	pstmt2->setString(2, device_id);
	pstmt2->executeUpdate();
	sql::ResultSet *res2 = pstmt2->executeQuery();
	delete pstmt2;
	while (res2->next()) {
		cout << "has next" << endl;
		unsigned long long from_id = res2->getUInt64("from_id");
		cout << "from_id = " << from_id << endl;
		string from_user = usernameFor(from_id);
		cout << "from_user = " << from_user << endl;
		if(from_user == "") {
			//Error - No user with this user_id found
			throw PilgrimException("No user with user_id found!");
		}
		unsigned int mid = res2->getUInt("message_id");
		cout << "got message id: " << mid << endl;
		Message m(mid, res2->getString("session_id"), from_user, res2->getString("from_device"),res2->getString("group_name"), res2->getString("text"));
		sl.addMessage(m);
		sql::PreparedStatement *storeMessageDeliveryStatement;
		try {
			cout << "storing: " << mid << ", " << device_id << " into message_delivery" << endl;
			storeMessageDeliveryStatement = con->prepareStatement("INSERT INTO message_delivery(message_id, to_device) values (?, ?)");
			storeMessageDeliveryStatement->setUInt(1, mid);
			storeMessageDeliveryStatement->setString(2, device_id);
			storeMessageDeliveryStatement->executeUpdate();
			storeMessageDeliveryStatement->executeQuery();
			//TODO For some reason, DB tries to insert twice and throws duplicate entry exception: ignoring for now.
		} catch(std::exception& e) {
			std::cerr<< "Ignoring DB exception: " << e.what() <<std::endl;
		}
		delete storeMessageDeliveryStatement;
	}
	return sl;
}
