#ifndef MYSQLCONNECTOR_H
#define MYSQLCONNECTOR_H

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cppconn/connection.h>

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

using namespace std;

class MySQLConnector {
public:
  MySQLConnector();
  ~MySQLConnector();
  sql::Connection *connect();
  void disconnect();
};

#endif
