#include <stdlib.h>
#include <iostream>
#include "MySQLConnector.h"

using namespace std;

MySQLConnector::MySQLConnector(){

}

sql::Connection *MySQLConnector::connect()
{

sql::Connection *con;

try {
  sql::Driver *driver;
  /* Create a connection */
  driver = get_driver_instance();
  ifstream file;
  file.open("db.conf");
  string location, user, password;
  file >> location >> user >> password;
  file.close();
  con = driver->connect(location, user, password);
  con->setSchema("safechat");

} catch (sql::SQLException &e) {
  cout << "# ERR: SQLException in " << __FILE__;
  cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
  cout << "# ERR: " << e.what();
  cout << " (MySQL error code: " << e.getErrorCode();
  cout << ", SQLState: " << e.getSQLState() << " )" << endl;
}
  return con;
};

