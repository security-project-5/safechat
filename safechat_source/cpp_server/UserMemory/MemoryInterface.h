/*
 * MemoryInterface.h
 *
 *  Created on: Feb 11, 2016
 *      Author: daniel
 */

#ifndef MEMORYINTERFACE_H_
#define MEMORYINTERFACE_H_

#include <vector>
#include <string>

#include "../PublicKey.h"
#include "../UserInfo.h"
#include "../SessionList.h"

class MemoryInterface {
public:
	virtual ~MemoryInterface() {}//interface does not have anything to destruct

	virtual bool verifyLogin(std::string username, std::string saltedHashOfPassword) = 0;

	virtual void addPublicKey(std::string username, std::string device_id, PublicKey) = 0;

	virtual PublicKey getPublicKey(std::string device_id) = 0;

	virtual UserInfo getUserAndDeviceInfo(std::string username) = 0;




	virtual void storeSessionInit(std::string from_username, std::string from_device, std::string to_device, std::string session_id, std::string msg) = 0;

	virtual void storeMessage(unsigned int message_id, std::string from_username, std::string from_device, std::string to_username, string group_name, std::string session_id, std::string msg) = 0;

	virtual SessionList getSessionsWithMsgsFor(std::string username, std::string device_id) = 0;

};

#endif /* MEMORYINTERFACE_H_ */
