/*
 * DB.h
 *
 *  Created on: Mar 21, 2016
 *      Author: jan
 */

#include "MemoryInterface.h"
#include <vector>
#include "../PublicKey.h"
#include "../UserInfo.h"
#include "MySQLConnector.h"
#include "../PilgrimException.h"
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include "../SessionList.h"
#include <climits>

class DB : public MemoryInterface {
  private:
	sql::Connection *con;
	unsigned long long userIdFor(string username);
	string usernameFor(unsigned long long username);

  public:
	DB();
	~DB();

	bool verifyLogin(string username, string saltedHashOfPassword);

	void addPublicKey(string username, string device_id, PublicKey);

	PublicKey getPublicKey(string device_id) throw(PilgrimException);

	UserInfo getUserAndDeviceInfo(string username) throw(PilgrimException);

	void storeSessionInit(string from_user, string from_device, string to_device, string session_id, string msg);

	void storeMessage(unsigned int message_id, string from_username, string from_device, string to_username, string group_name, string session_id, string msg);

	SessionList getSessionsWithMsgsFor(string username, string device_id)throw(PilgrimException);
};
