/*
 * SessionList.h
 *
 *  Created on: Mar 21, 2016
 *      Author: daniel
 */

#ifndef SESSIONLIST_H_
#define SESSIONLIST_H_
#include <vector>
#include "SecurityModule/SecurityInterface.h"
#include "PublicKey.h"
#include "Session.h"
#include "Message.h"

class SessionList {
public:
	SessionList();
	virtual ~SessionList();
	std::string encode(SecurityInterface*, PublicKey) const;
	void addSession(Session);
	void addMessage(Message);
private:
	std::vector<Session> sessions;
	std::vector<Message> messages;
};

#endif /* SESSIONLIST_H_ */
