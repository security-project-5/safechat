/*
 * UserInfo.h
 *
 *  Created on: Mar 21, 2016
 *      Author: daniel
 */

#ifndef USERINFO_H_
#define USERINFO_H_

#include <string>
#include <vector>
#include "DeviceInfo.h"

class UserInfo {
public:
	UserInfo();
	UserInfo(string username, string firstname, string lastname, string email, string phone);
	std::string encode() const;
	void addDevice(DeviceInfo);
private:
	std::string username;
	std::string phone_number;
	std::string firstname;
	std::string lastname;
	std::string email;
	std::vector<DeviceInfo> devices;
};

#endif /* USERINFO_H_ */
