/*
 * SessionList.cpp
 *
 *  Created on: Mar 21, 2016
 *      Author: daniel
 */

#include "SessionList.h"

SessionList::SessionList() {
	// TODO Auto-generated constructor stub

}

SessionList::~SessionList() {
	// TODO Auto-generated destructor stub
}

std::string SessionList::encode(SecurityInterface* security, PublicKey recip_key) const {
	std::string enc = "";
	string temp;

	for(size_t i=0;i<sessions.size();i++){
		temp = sessions[i].encode(security,recip_key);
		enc += temp;
		cout << "Session " << i << " <" << temp << ">" << endl;
	}
	enc += NEW_LINE;//end of sessions
	for(size_t i=0;i<messages.size();i++){
		temp = messages[i].encode(security,recip_key);
		enc += temp;
		cout << "Message " << i << " <" << temp << ">" << endl;
	}
	enc += NEW_LINE;//end of messages
	cout << "Sending: " << sessions.size() << " session packets. " << " and: " << messages.size() << " message packets." << endl;
	return enc;
}

void SessionList::addMessage(Message m){
	messages.push_back(m);
}

void SessionList::addSession(Session s){
	sessions.push_back(s);
}
