/*
 * Msg_type.h
 *
 *  Created on: Feb 21, 2016
 *      Author: daniel
 */

#ifndef MSG_TYPE_H_
#define MSG_TYPE_H_

enum MSG_TYPE { LOGIN,//includes giving public key
			RESPONSE_TO_LOGIN_AND_KEY,

			//REQUEST_USERS,
			//LIST_OF_USERS,

			ADD_CONTACT,
			GET_PUBLIC_KEYS,

			SEND_SESSION_START,
			SEND_MESSAGE,

			CHECK_INBOX,
			GIVE_MESSAGES,

			___LAST_ENTRY_COUNT,
};



#endif /* MSG_TYPE_H_ */
