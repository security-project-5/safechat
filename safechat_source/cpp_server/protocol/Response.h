/*
 * Response.h
 *
 *  Created on: Feb 21, 2016
 *      Author: daniel
 */

#ifndef RESPONSE_H_
#define RESPONSE_H_

#include <string>
#include <iostream>
#include <sstream>
#include <ctime>
#include <cstdlib>
#include <chrono>

#include "Msg_type.h"
#include "../PublicKey.h"
#include "../SecurityModule/SecurityInterface.h"


class Response {
public:
	Response();
	virtual ~Response();
	void setMsgType(MSG_TYPE type);
	void addToken(std::string);

	std::string encode(SecurityInterface* security, PublicKey client_key) const;

private:
	MSG_TYPE type;
	std::string data;

	int getNewMesageId() const;
	std::string getTime() const ;
};

#endif /* RESPONSE_H_ */
