#include "Request.h"

const unsigned int RCVBUFSIZE = 5;    // Size of receive buffer

Request::Request(SecurityInterface* security, string fullMessage) throw (PilgrimException) {
	cout << "create request object" << endl;
	message_stream = new istringstream(fullMessage);

	payload = getAndPieceTogetherPayload(message_stream, security);
	cout<<"payload <"<<payload<<">"<<endl;
	istream * data_stream = new istringstream(payload);
	message_id = Utility::getTokenAsUInt(data_stream);
	cout<<"messageid = <"<<message_id<<">"<<endl;
	//TODO check on valid msg_id ?
	type = Request::getTokenAsReqType(data_stream);
	cout<<"message type <"<<type<<">"<<endl;
	std::string timestamp = Utility::getToken(data_stream);
	cout<<"timestamp <"<<timestamp<<">"<<endl;
	data = "";
	std::string bit_data = "";
	do {
		bit_data = Utility::getToken(data_stream);
		data += bit_data;
		data += '\n';
	}while(bit_data != "");
	cout<<"finished with data: <"<<data<<">"<<endl;
}

std::string Request::getData(){
	return data;
}

std::string Request::getPayload(){
	return payload;
}

unsigned int Request::getMessageID() {
	return message_id;
}

MSG_TYPE Request::getType(){
	return type;
}

istream* Request::getMessageStream() {
	return message_stream;
}

std::string Request::getAndPieceTogetherPayload(istream* message_stream, SecurityInterface* security){
	std::string partial = Utility::getToken(message_stream);
	std::string payload = "";
	cout << "Partial<" << partial << ">" << endl;
	while(partial != ""){
		//decrypt
		std::string dec_part = security->accept(partial);

		//add to payload
		payload += dec_part;

		//get again
		partial = Utility::getToken(message_stream);
		cout << "Partial<" << partial << ">" << endl;
	}
	cout << "Payload<" << payload << ">" << endl;
	return payload;
}

MSG_TYPE Request::getTokenAsReqType(istream* in) {

	unsigned int uint = Utility::getTokenAsUInt(in);

	if(uint >= ___LAST_ENTRY_COUNT){
		throw PilgrimException("invalid message type");
	}
	return (MSG_TYPE) uint;
}
