/*
 * Response.cpp
 *
 *  Created on: Feb 21, 2016
 *      Author: daniel
 */

#include "Response.h"

Response::Response() {
	// TODO Auto-generated constructor stub
	srand(time(0));
}

Response::~Response() {
	// TODO Auto-generated destructor stub
}

void Response::setMsgType(MSG_TYPE type){
	this->type = type;
}

void Response::addToken(std::string str){
	this->data += str + '\n';
}
/**
 * encrypt and sign
 */
std::string Response::encode(SecurityInterface* security, PublicKey client_key) const {
	std::string payload = "";
	std::ostringstream out;
	//header stuff
	//type
	//TODO encode header
	out<<getNewMesageId()<<"\n";
	out<<(unsigned int)type <<"\n";//type id
	out<<getTime()<< "\n";//timestamp

	if(type != GIVE_MESSAGES){
		out<<this->data + "\n";
	}
	payload = out.str();
	std::cout<<"response payload <"<<payload<<">"<<std::endl;
	//encrypt
	std::string full_msg = security->encrypt(payload, client_key) + "\n";
	full_msg += security->sign(payload) + "\n" + "\r";

	return full_msg;
}


//private
int Response::getNewMesageId() const{
	std::stringstream ss;
	ss<<std::time(0)<<rand();
	int n;
	ss >> n;
	return n;
}

std::string Response::getTime() const {
	unsigned long milliseconds_since_epoch =
	    std::chrono::duration_cast<std::chrono::milliseconds>
	        (std::chrono::system_clock::now().time_since_epoch()).count();
	std::stringstream ss;
	ss << milliseconds_since_epoch;
	return ss.str();
}
