#ifndef PROTOCOL_REQUEST
#define PROTOCOL_REQUEST
#include <string>

#include "../PracticalSocket.h"  // For Socket, ServerSocket, and SocketException
#include "../Utility.h" // for tokenizing functions
#include "../PilgrimException.h"
#include "../SecurityModule/SecurityInterface.h"
#include "Msg_type.h"



class Request {
	public:
	Request(SecurityInterface* security, string fullMessage) throw (PilgrimException);
	std::string getData();
	std::string getPayload();
	MSG_TYPE getType();
	istream* getMessageStream();
	unsigned int getMessageID();

	private:
	long message_id;

	std::istream * message_stream;
	std::string payload;
	std::string data;
	MSG_TYPE type;

	static MSG_TYPE getTokenAsReqType(istream*);
	//TODO combine for code quality

	std::string getAndPieceTogetherPayload(istream* message_stream, SecurityInterface* security);

};
#endif
