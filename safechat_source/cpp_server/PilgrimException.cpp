/*
 * PilgrimException.cpp
 *
 *  Created on: Feb 18, 2016
 *      Author: daniel
 */

#include "PilgrimException.h"

PilgrimException::PilgrimException() {
	message = "Generic Pilgrim exception";
}

PilgrimException::PilgrimException(std::string message) {
	this->message = message;
}

PilgrimException::~PilgrimException() throw(){

}

const char* PilgrimException::what() const throw(){
	return message.c_str();
}

