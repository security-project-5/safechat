/*
 * PublicKey.h
 *
 *  Created on: Feb 15, 2016
 *      Author: daniel
 */

#ifndef PUBLICKEY_H_
#define PUBLICKEY_H_

#include <string>
struct PublicKey {

	PublicKey(std::string = "");
	std::string _64_byte_array;
};


#endif /* PUBLICKEY_H_ */
