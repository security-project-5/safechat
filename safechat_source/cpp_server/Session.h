/*
 * Session.h
 *
 *  Created on: Mar 21, 2016
 *      Author: daniel
 */

#ifndef SESSION_H_
#define SESSION_H_

#include <string>

#include "PublicKey.h"
#include "SecurityModule/SecurityInterface.h"
#include "Utility.h"

class Session {
public:
	Session();
	Session(string id, string from_user, string send_dev_id, string key_data);
	virtual ~Session();
	std::string encode(SecurityInterface*,PublicKey) const;
private:
	std::string id;
	std::string sender_device_id;
	std::string key_data;
	std::string from_user;
};

#endif /* SESSION_H_ */
