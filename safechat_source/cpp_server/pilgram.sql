-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Počítač: sql3.freesqldatabase.com
-- Vygenerováno: Úte 22. bře 2016, 12:05
-- Verze MySQL: 5.5.46-0ubuntu0.12.04.2
-- Verze PHP: 5.3.28

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `sql3107916`
--

-- --------------------------------------------------------
-- --------------------------------------------------------

DROP TABLE IF EXISTS login_attempt;
DROP TABLE IF EXISTS message_delivery;
DROP TABLE IF EXISTS session_delivery;
DROP TABLE IF EXISTS message;
DROP TABLE IF EXISTS session;
DROP TABLE IF EXISTS session_cookie;
DROP TABLE IF EXISTS user_device;
DROP TABLE IF EXISTS user_profile_photo;
DROP TABLE IF EXISTS user;

--
-- Struktura tabulky `user`
--
CREATE TABLE IF NOT EXISTS user (
  user_id bigint(20) NOT NULL AUTO_INCREMENT,
  firstname varchar(80) DEFAULT NULL,
  lastname text,
  phone varchar(10) DEFAULT NULL,
  email varchar(40) UNIQUE NOT NULL,
  password varchar(100) NOT NULL,
  registered_timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (user_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS user_profile_photo (
  user_id bigint(20) NOT NULL,
  filename varchar(40) NOT NULL,
  KEY user_id (user_id),
  FOREIGN KEY (user_id) REFERENCES user (user_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Struktura tabulky `login_attempt`
--
CREATE TABLE IF NOT EXISTS login_attempt (
  user_id bigint(20) references user,
  attempts int(11) NOT NULL,
  ip_addr text NOT NULL,
  timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY fk_user_id(user_id)
  REFERENCES user(user_id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_device`
--

CREATE TABLE IF NOT EXISTS user_device (
  user_id bigint(20) NOT NULL,
  device_id varchar(36) NOT NULL,
  public_key text NOT NULL,
  PRIMARY KEY(device_id),
  FOREIGN KEY(user_id) REFERENCES user(user_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Struktura tabulky `session`
--
CREATE TABLE IF NOT EXISTS session (
  session_id varchar(36) NOT NULL,
  from_id bigint(20) NOT NULL,
  from_device varchar(36) NOT NULL,
  to_device varchar(36) NOT NULL,
  msg TEXT NOT NULL,
  UNIQUE(session_id, to_device),
  FOREIGN KEY(from_id) REFERENCES user(user_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY(from_device) REFERENCES user_device(device_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY(to_device) REFERENCES user_device(device_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `session_cookie`
--
CREATE TABLE IF NOT EXISTS session_cookie (
  email varchar(40) NOT NULL,
  created_timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  duration_seconds int(11) NOT NULL DEFAULT '60',
  cookie varchar(100) NOT NULL,
  KEY (email)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Struktura tabulky `message`
--
CREATE TABLE IF NOT EXISTS message (
  message_id int unsigned NOT NULL,
  from_id bigint(20) NOT NULL,
  from_device varchar(36) NOT NULL,
  to_id bigint(20) NOT NULL,
  group_name varchar(50) NOT NULL,
  session_id varchar(36) NOT NULL,
  text TEXT NOT NULL,
  sent_timestamp timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  read_timestamp timestamp NULL DEFAULT NULL,
  PRIMARY KEY(message_id),
  FOREIGN KEY (session_id) REFERENCES session(session_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (from_device) REFERENCES user_device(device_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (from_id) REFERENCES user(user_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (to_id) REFERENCES user(user_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS message_delivery (
  message_id int unsigned NOT NULL,
  to_device varchar(36) NOT NULL,
  FOREIGN KEY (message_id) REFERENCES message(message_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (to_device) REFERENCES user_device(device_id) ON DELETE CASCADE ON UPDATE CASCADE,
  UNIQUE(message_id, to_device)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS session_delivery (
  session_id varchar(36) NOT NULL,
  to_device varchar(36) NOT NULL,
  FOREIGN KEY (session_id) REFERENCES session(session_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (to_device) REFERENCES user_device(device_id) ON DELETE CASCADE ON UPDATE CASCADE,
  UNIQUE(session_id, to_device)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
