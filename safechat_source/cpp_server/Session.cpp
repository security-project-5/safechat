/*
 * Session.cpp
 *
 *  Created on: Mar 21, 2016
 *      Author: daniel
 */

#include "Session.h"

Session::Session() {
	// TODO Auto-generated constructor stub

}

Session::Session(string id, string from_user, string send_dev_id, string key_data){
	this->id = id;
	this->from_user = from_user;
	this->sender_device_id = send_dev_id;
	this->key_data = key_data;
}

Session::~Session() {
	// TODO Auto-generated destructor stub
}

std::string Session::encode(SecurityInterface* security, PublicKey recip_key) const{

	std::string session_packet_payload = id + NEW_LINE;
	session_packet_payload += from_user + NEW_LINE;
	session_packet_payload += sender_device_id + NEW_LINE;

	std::string session_packet = security->encrypt(session_packet_payload, recip_key) + NEW_LINE;
	session_packet += security->sign(session_packet_payload) + NEW_LINE;
	session_packet += key_data;

	return session_packet;
}

