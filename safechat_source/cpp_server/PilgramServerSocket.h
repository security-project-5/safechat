/*
 * PilgramServerSocket.h
 *
 *  Created on: Mar 28, 2016
 *      Author: daniel
 */

#ifndef PILGRAMSERVERSOCKET_H_
#define PILGRAMSERVERSOCKET_H_

#include <map>
#include "PracticalSocket.h"
#include "UserMemory/DB.h"
#include "SecurityModule/RealSecurity.h"
#include "PilgramClient.h"

class PilgramServerSocket : public TCPServerSocket {
public:
	PilgramServerSocket(int port);
	void select() throw (SocketException);

private:
	int maxDesc;
	MemoryInterface* memory;
	SecurityInterface* security;
	fd_set readset;
	int selectCount;
};

#endif /* PILGRAMSERVERSOCKET_H_ */
