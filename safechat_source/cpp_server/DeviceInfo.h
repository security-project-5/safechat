/*
 * DeviceInfo.h
 *
 *  Created on: Mar 21, 2016
 *      Author: daniel
 */

#ifndef DEVICEINFO_H_
#define DEVICEINFO_H_
#include <string>
#include "PublicKey.h"
#include "Utility.h"


class DeviceInfo {
public:
	DeviceInfo();
	DeviceInfo(PublicKey key, std::string id);
	virtual ~DeviceInfo();
	std::string encode() const;
private:
	PublicKey key;
	std::string id;
};

#endif /* DEVICEINFO_H_ */
