/*
 *   C++ sockets on Unix and Windows
 *   Copyright (C) 2002
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "PilgramServerSocket.h"
#include <iostream>           // For cerr and cout
#include <cstdlib>            // For atoi()
#include <pthread.h>

using namespace std;


int main(int argc, char *argv[]) {
	if (argc != 2) {                     // Test for correct number of arguments
		cerr << "Usage: " << argv[0] << " <Server Port>" << endl;
		exit(1);
	}
	unsigned short echoServPort = atoi(argv[1]);  // First arg: local port

	PilgramServerSocket servSock(echoServPort);     // Server Socket object
	bool done = false;
    while(!done) {   // Run forever
    	try {
    		servSock.select();
		} catch (exception& ex) {
			cerr << ex.what() << endl;
		} catch(...) {
			cerr << "Caught Unknown Exception" << endl;
		}
    }
    servSock.close();
    // NOT REACHED

    return 0;
}
