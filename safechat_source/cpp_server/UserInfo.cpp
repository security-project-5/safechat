/*
 * UserInfo.cpp
 *
 *  Created on: Mar 21, 2016
 *      Author: daniel
 */

#include "UserInfo.h"

UserInfo::UserInfo(){
	username = "null";
	firstname = "null";
	lastname = "null";
	email = "null";
	phone_number = "null";
}

UserInfo::UserInfo(string un, string fn, string ln, string em, string pn) {
	username = un;
	firstname = fn;
	lastname = ln;
	email = em;
	phone_number = pn;
}

void UserInfo::addDevice(DeviceInfo dev){
	devices.push_back(dev);
}


std::string UserInfo::encode() const {
	std::string enc = "";

	enc += username + NEW_LINE;
	enc += firstname + NEW_LINE;
	enc += lastname + NEW_LINE;
	enc += phone_number + NEW_LINE;
	enc += email + NEW_LINE;

	for(size_t i =0;i<devices.size();i++){
		enc += devices[i].encode() + NEW_LINE;
	}
	enc += NEW_LINE; // done with devices

	return enc;
}

