/*
 * RealSecurity.cpp
 *
 *  Created on: Mar 20, 2016
 *      Author: daniel
 */

#include "RealSecurity.h"

RealSecurity::RealSecurity() {
	Botan::LibraryInitializer init;
	// TODO Auto-generated constructor stub
	rng = new AutoSeeded_RNG();//this is eclips's fault that there is an error

	ifstream file;
	file.open("key.conf");
	string passphrase;
	file >> passphrase;
	file.close();
	Private_Key* temp_pk = PKCS8::load_key("private.pem", *rng, passphrase);
	AlgorithmIdentifier alg_id("RSA", AlgorithmIdentifier::USE_NULL_PARAM);


	RSA_PrivateKey pk(alg_id, temp_pk->pkcs8_private_key(), *rng);
	max_msg_size_asym = pk.max_input_bits()/8;
	std::string pub = X509::PEM_encode(pk);
	std::string priv = PKCS8::PEM_encode(pk);


	//DataSource_Memory key_pub(pub);
	DataSource_Memory key_priv(priv);

	//X509_PublicKey* pub_rsa = X509::load_key(key_pub);
	PKCS8_PrivateKey* priv_rsa = PKCS8::load_key(key_priv, *rng);

	//PK_Encrypting_Key* enckey = dynamic_cast<PK_Encrypting_Key*>(pub_rsa);
	//PK_Decyrpting_Key* deckey = dynamic_cast<PK_Decrypting_Key*>(priv_rsa);

	dec = new PK_Decryptor_EME(*priv_rsa, "EME-PKCS1-v1_5");//"EME1(SHA-256)"
	signer = new PK_Signer(*priv_rsa, "EMSA3(SHA-256)");
	std::cout<<"done initializing security"<<std::endl;

}

RealSecurity::~RealSecurity() {
	delete dec;
	delete rng;
	delete signer;
}


/*
 * assumptions: base64 encoded encryption
 */
std::string RealSecurity::accept(std::string str){

	secure_vector<byte> ciphertext = safeDecode(str);//ignore whitespace

	secure_vector<byte> decrypted;
	try{
		decrypted = dec->decrypt(ciphertext);
	}catch(exception& e){
		//TODO exception
		cerr<<"error while decrypting: "<<e.what()<<endl;
		//throw PilgrimException("error while decrypting: "+e.what());
	}

	return std::string(decrypted.begin(), decrypted.end());
}

bool RealSecurity::verify_hash(std::string msg, std::string hash){
	secure_vector<byte> hash_bytes = safeDecode(hash);
	HashFunction* hasher = new SHA_256();

	bool match = true;
	secure_vector<byte> computed = hasher->process(msg);
	if(hash_bytes.size() != computed.size()){
		match = false;
	}
	for(size_t i=0;i<computed.size() && match; i++){
		if(hash_bytes[i] != computed[i]){
			match = false;
		}
	}
	delete hasher;
	return match;
}

std::string RealSecurity::encrypt(std::string msg, PublicKey key){
	Public_Key* pub = getActualKey(key);
	PK_Encryptor* enc = new PK_Encryptor_EME(*pub, "EME-PKCS1-v1_5");
	secure_vector<byte> msg_bytes(msg.data(), msg.data()+msg.length());

	std::string chunks_64_encry = "";
	size_t max_size = enc->maximum_input_size();
	//for
	size_t numNormalChunks = msg_bytes.size()/max_size;
	for(size_t ch =0 ;ch < numNormalChunks; ch++){
		secure_vector<byte> chunk_pt(msg_bytes.begin()+ch*max_size, msg_bytes.begin() + (ch+1)*max_size);
		vector<byte> enc_bytes = enc->encrypt(chunk_pt, *rng);
		chunks_64_encry += base64_encode(enc_bytes) + '\n';
	}
	//deal with last uneven chunk
	secure_vector<byte> last_chunk (msg_bytes.begin()+numNormalChunks*max_size, msg_bytes.end());


	if(last_chunk.size() > 0){
		vector<byte> last_enc = enc->encrypt(last_chunk, *rng);
		chunks_64_encry += base64_encode(last_enc) + '\n';
	}

	delete enc;
	delete pub;
	return chunks_64_encry;
}

bool RealSecurity::verify(std::string msg, std::string sig, PublicKey key){
	//decyrpt using the client's public key
	cout << "msg:<" << msg << ">" << endl;
	cout << "sig:<" << sig << ">" << endl;
	cout << "key:<" << key._64_byte_array << ">" << endl;

	Public_Key* pub = getActualKey(key);
	cout << "got actual public key" << endl;
	PK_Verifier* ver = new PK_Verifier(*pub, "EMSA3(SHA-256)");

	secure_vector<byte> sig_bytes = safeDecode(sig);//ignore whitespace

	secure_vector<byte> msg_bytes(msg.data(), msg.data()+msg.length());

	return ver->verify_message(msg_bytes, sig_bytes);
}

std::string RealSecurity::sign(std::string msg){
	secure_vector<byte> msg_bytes(msg.data(), msg.data()+msg.length());

	vector<byte> sig_bytes = signer->sign_message(msg_bytes, *rng);

	return base64_encode(sig_bytes);
}


Public_Key* RealSecurity::getActualKey(PublicKey key) const{

	secure_vector<byte> pub_arr = safeDecode(key._64_byte_array);
	std::vector<byte> pub_n_arr;
	for(size_t i=0;i<pub_arr.size();i++){
		pub_n_arr.push_back(pub_arr[i]);
	}
	Public_Key* pub = X509::load_key(pub_n_arr);
	//debug if neccessary
	return pub;
}

secure_vector<byte> RealSecurity::safeDecode(const std::string& input) const throw (PilgrimException) {
	static const boost::regex base64("([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)");
	if(boost::regex_match(input, base64)) {
		return base64_decode(input, true);
	}
	throw PilgrimException("Invalid base64");
}
