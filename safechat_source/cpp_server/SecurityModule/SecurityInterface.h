#ifndef SECURITYINTERFACE_H_
#define SECURITYINTERFACE_H_

#include <string>
#include "../PublicKey.h"

class SecurityInterface {
public:
	virtual ~SecurityInterface() {}

	virtual std::string sign(std::string) = 0;
	virtual std::string accept(std::string) = 0;

	virtual bool verify_hash(std::string msg, std::string hash) = 0;

	virtual std::string encrypt(std::string msg, PublicKey key) = 0;
	virtual bool verify(std::string msg, std::string signature, PublicKey key) = 0;
};

#endif
