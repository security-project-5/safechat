/*
 * RealSecurity.h
 *
 *  Created on: Mar 20, 2016
 *      Author: daniel
 */

#ifndef REALSECURITY_H_
#define REALSECURITY_H_

#include "SecurityInterface.h"
#include "../Utility.h"


#include <botan/rsa.h>
#include <botan/auto_rng.h>
#include <botan/pkcs8.h>
#include <botan/botan.h>
#include <botan/init.h>
#include <botan/pubkey.h>
#include <botan/base64.h>
#include <botan/sha2_32.h>

#include <boost/regex.hpp>

#include <fstream>
#include <sstream>
#include <vector>
#include <regex>
#include <string>
#include <iostream>

using namespace Botan;
class RealSecurity: public SecurityInterface {
public:
	RealSecurity();
	virtual ~RealSecurity();

	std::string sign(std::string);
	std::string accept(std::string);

	bool verify_hash(std::string msg, std::string hash);

	std::string encrypt(std::string msg, PublicKey key);
	bool verify(std::string msg, std::string, PublicKey key);
private:
	PK_Decryptor* dec;
	PK_Signer* signer;
	unsigned int max_msg_size_asym;
	RandomNumberGenerator* rng;

	Public_Key* getActualKey(PublicKey my_key) const;
	secure_vector<byte> safeDecode(const std::string& input) const throw (PilgrimException);
};

#endif /* REALSECURITY_H_ */
