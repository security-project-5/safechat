/*
 * PilgramClient.cpp
 *
 *  Created on: Mar 28, 2016
 *      Author: daniel
 */

#include "PilgramClient.h"

PilgramClient::PilgramClient(TCPSocket* clientSocket, MemoryInterface* memory, SecurityInterface* security) {
	this->clientSocket = clientSocket;
	this->memory = memory;
	this->security = security;
	msgReceivedSoFar = "";
}

bool PilgramClient::receiveMore() throw (PilgrimException)  {
	cout << "receiveMore" << endl;
	const char DELIM = '\r';
	int result = -1;
	do {
		result = clientSocket->recv(buf, CONTROL_BUFFER_SIZE);
		cout << "Recv result:" << result << endl;
	} while(result == -1);
	if(result == 0) {
		cout << "Result was 0" << endl;
		return false;
	}
	else if(result < 0) {
		cout << "Result was less than 0: " << result << endl;
		return false;
	}
	cout << "append, result was: " << result << endl;
	msgReceivedSoFar.append(buf, result);
	cout << "done append" << endl;
	while((result = msgReceivedSoFar.find_first_of(DELIM)) != string::npos) {
		cout << "Have a full message:<" << msgReceivedSoFar << ">" << endl;
		string toSend = respond(msgReceivedSoFar.substr(0, result));
		msgReceivedSoFar = msgReceivedSoFar.substr(result+1);
		if(!toSend.empty()) {
			clientSocket->send(toSend.c_str(), toSend.size());
		}
	}
	cout << "End receiveMore" << endl;
	return true;
}

string PilgramClient::respond(string fullMessage) {
	try{
	Request r(security, fullMessage);

	const string NOT_IMPLEMENTED_ERROR_MSG = "This function is not supported\n";

	switch(r.getType()){
	case LOGIN:
		return handleLogin(&r);
		break;
	/*case REQUEST_USERS:
		handleUserRequest(sock,&r,memory,security);
		break;*/
	case ADD_CONTACT:
		return handleAddContact(&r);
		break;
	case SEND_SESSION_START:
		handleSessionStart(&r);
		return "";
		break;
	case SEND_MESSAGE:
		handleMsg(&r);
		return "";
		break;
	case CHECK_INBOX:
		return handleInboxPull(&r);
		break;

	default:
		return NOT_IMPLEMENTED_ERROR_MSG;
		break;
	}
	}catch(std::exception& e){
		std::cerr<< "Exception: " << e.what() <<std::endl;
		return "";
	}
	return "";
}



string PilgramClient::handleLogin(Request* r){
	//assume payload has already been collected
	string client_msg = "";
	string payload_hash = Utility::getToken(r->getMessageStream());
	string payload =  r->getPayload();
	bool valid = security->verify_hash(payload, payload_hash);
	if(!valid){
		throw PilgrimException("hash does not match");
	}

	istream* in = new istringstream(r->getData());

	string safechat_version = Utility::getToken(in);
	string username;
	if(safechat_version == SAFECHAT_VERSION) {
		username = Utility::getToken(in);
	}
	else {
		client_msg = "You you using an outdated version of SafeChat, and need to update to the newest version. You can find it at: https://192.168.5.64";
		static const boost::regex safechat("SafeChat");
		if(!boost::regex_search(safechat_version, safechat)) {
			username = safechat_version;
		}
		else {
			username = Utility::getToken(in);
		}
	}
	std::cout<<"got username <"<<username<<">"<<std::endl;
	string salted_hash_pw = Utility::getToken(in);
	std::cout<<"got pw <"<<salted_hash_pw<<">"<<std::endl;
	bool match = memory->verifyLogin(username, salted_hash_pw);
	std::string device_id = Utility::getToken(in);
	//TODO store device id
	PublicKey* pub = new PublicKey(Utility::getToken(in));
	string _64_use_username = Utility::getToken(in);
	if(match){
		std::cout<<"good login"<<std::endl;

		if(security->verify(username, _64_use_username, *pub)){
			memory->addPublicKey(username,device_id, *pub);
			if(client_msg == "") client_msg = "ACCEPT";
		}
		else {
			match = false;
			client_msg = "You are using an invalid key pair.";
		}
	}
	else{
		client_msg = "This is an invalid username and password combination.";
	}
	Response res;
	res.setMsgType(RESPONSE_TO_LOGIN_AND_KEY);
	res.addToken(client_msg);

	string enc = res.encode(security, *pub);
	delete pub;
	return enc;
}

string PilgramClient::handleAddContact(Request* r){
	cout << "handle add contact" << endl;
	string payload_hash = Utility::getToken(r->getMessageStream());

	istream* in = new istringstream(r->getData());
	//get what is neccessary to check the signature
	string username = Utility::getToken(in);
	string device_id = Utility::getToken(in);
	string username_to_get = Utility::getToken(in);

	PublicKey pub = memory->getPublicKey(device_id);
	cout << "got public key" << endl;
	if(!security->verify(r->getPayload(), payload_hash, pub)){
		throw PilgrimException("invalid hash of payload");
	}
	cout << "verified" << endl;

	//process request
	UserInfo requestedInfo;
	Response res;
	res.setMsgType(GET_PUBLIC_KEYS);
	try{
		requestedInfo = memory->getUserAndDeviceInfo(username_to_get);
		res.addToken(requestedInfo.encode());
		cout << "got user and device info<"<<requestedInfo.encode()<<">" << endl;
	}catch(PilgrimException& e){
		res.addToken("INVALID_USERNAME");
	}

	return res.encode(security, pub);
}

void PilgramClient::handleSessionStart(Request* r){
	string payload_hash = Utility::getToken(r->getMessageStream());

	istream* in = new istringstream(r->getData());
	string session_id = Utility::getToken(in);
	string sender_username = Utility::getToken(in);
	string sender_device_id = Utility::getToken(in);
	string recip_device_id = Utility::getToken(in);
	string encrypted_key_data = "";
	string part = Utility::getToken(in);
	while(part != "") {
		encrypted_key_data += part + "\n";
		part = Utility::getToken(in);
	}
	encrypted_key_data += "\n";

	PublicKey pub = memory->getPublicKey(sender_device_id);

	if(!security->verify(r->getPayload(), payload_hash, pub)){
		throw PilgrimException("invalid hash of payload");
	}

	//process request (no response)
	cout << "calling memory->storeSessionInit()" << endl;
	memory->storeSessionInit(sender_username, sender_device_id, recip_device_id, session_id, encrypted_key_data);

}

void PilgramClient::handleMsg(Request* r){
	string payload_hash = Utility::getToken(r->getMessageStream());

	istream* in = new istringstream(r->getData());
	string session_id = Utility::getToken(in);
	string sender_username = Utility::getToken(in);
	string sender_device_id = Utility::getToken(in);
	string recipient_username = Utility::getToken(in);
	string group_name = Utility::getToken(in);

	PublicKey pub = memory->getPublicKey(sender_device_id);

	if(!security->verify(r->getPayload(), payload_hash, pub)){
		throw PilgrimException("invalid hash of payload");
	}

	//process request (no response)

	string msg_payload = Utility::getToken(r->getMessageStream());
	string msg_signature = Utility::getToken(r->getMessageStream());
	string msg_cat = msg_payload + NEW_LINE + msg_signature + NEW_LINE;
	memory->storeMessage(r->getMessageID(), sender_username, sender_device_id, recipient_username, group_name, session_id, msg_cat);

}

string PilgramClient::handleInboxPull(Request* r){
	string payload_hash = Utility::getToken(r->getMessageStream());

	istream* in = new istringstream(r->getData());
	string sender_username = Utility::getToken(in);
	string sender_device_id = Utility::getToken(in);

	PublicKey pub = memory->getPublicKey(sender_device_id);

	if(!security->verify(r->getPayload(), payload_hash, pub)){
		throw PilgrimException("invalid hash of payload");
	}

	SessionList sl = memory->getSessionsWithMsgsFor(sender_username, sender_device_id);
	Response res;
	res.setMsgType(GIVE_MESSAGES);
	string simple_enc = res.encode(security, pub);
	cout<<"simple enc <"<<simple_enc<<">"<<endl;
	//now add session list to enc
	simple_enc = simple_enc.substr(0, simple_enc.size()-1);
	string sl_enc = sl.encode(security, pub);
	string enc = simple_enc + sl_enc + "\r";
	return enc;
}
