/*
 * PilgrimException.h
 *
 *  Created on: Feb 18, 2016
 *      Author: daniel
 */

#ifndef PILGRIMEXCEPTION_H_
#define PILGRIMEXCEPTION_H_

#include <exception>
#include <string>

class PilgrimException: public std::exception {
public:
	PilgrimException();
	PilgrimException(std::string msg);
	virtual ~PilgrimException() throw();
	virtual const char* what() const throw();
private:
	std::string message;
};

#endif /* PILGRIMEXCEPTION_H_ */
