/*
 * PilgramClient.h
 *
 *  Created on: Mar 28, 2016
 *      Author: daniel
 */

#ifndef PILGRAMCLIENT_H_
#define PILGRAMCLIENT_H_

#include <boost/regex.hpp>
#include <string>
#include "PracticalSocket.h"
#include "UserMemory/MemoryInterface.h"
#include "SecurityModule/SecurityInterface.h"
#include "protocol/Request.h"
#include "protocol/Response.h"

using namespace std;

const int CONTROL_BUFFER_SIZE = 10000;

class PilgramClient {
public:
	PilgramClient(TCPSocket* clientSocket, MemoryInterface* memory, SecurityInterface* security);
	bool receiveMore() throw (PilgrimException);
	TCPSocket* clientSocket;

private:
	MemoryInterface* memory;
	SecurityInterface* security;
	string msgReceivedSoFar;
	const string SAFECHAT_VERSION = "SafeChat2.0";
	char buf[CONTROL_BUFFER_SIZE+1];

	string respond(string fullMessage);

	string handleLogin(Request* r);
	string handleAddContact(Request* r);
	void handleSessionStart(Request* r);
	void handleMsg(Request* r);
	string handleInboxPull(Request* r);
};

#endif /* PILGRAMCLIENT_H_ */
