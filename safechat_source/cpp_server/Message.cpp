/*
 * Message.cpp
 *
 *  Created on: Mar 21, 2016
 *      Author: daniel
 */

#include "Message.h"

Message::Message() {
	// TODO Auto-generated constructor stub

}

Message::Message(int message_id, string sess_id, string send_user, string send_dev_id, string group_name, string msg_data){
	this->message_id = message_id;
	this->session_id = sess_id;
	this->sender_username = send_user;
	this->sender_device_id = send_dev_id;
	this->group_name = group_name;
	this->message_data = msg_data;
}

Message::~Message() {
	// TODO Auto-generated destructor stub
}

std::string Message::encode(SecurityInterface* security, PublicKey recip_key) const {
	std::string msg_header = session_id + NEW_LINE;
	msg_header += sender_username + NEW_LINE;
	msg_header += sender_device_id + NEW_LINE;
	msg_header += group_name + NEW_LINE;

	std::string msg_packet = security->encrypt(msg_header, recip_key) + NEW_LINE;
	msg_packet += security->sign(msg_header) + NEW_LINE;
	msg_packet += message_data;

	return msg_packet;
}

