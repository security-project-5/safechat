/*
 * Utility.h
 *
 *  Created on: Feb 10, 2016
 *      Author: daniel
 */

#ifndef UTILITY_H_
#define UTILITY_H_

#include <iostream> //for debuging
#include <sstream> // for type casting in getToken functions
#include <string> // for fill_n function
#include "PracticalSocket.h"
#include "PilgrimException.h"

using namespace std;

const char NEW_LINE = '\n';

class Utility {
public:

	static string getToken(istream* stream);
	static unsigned int getTokenAsUInt(istream* stream);
private:
	static unsigned int strToUint(string);
};


#endif /* UTILITY_H_ */
