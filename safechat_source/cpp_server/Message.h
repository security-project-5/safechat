/*
 * Message.h
	* changed from new system
 *
 *  Created on: Mar 21, 2016
 *      Author: daniel
 */

#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <string>
#include "PublicKey.h"
#include "Utility.h"
#include "SecurityModule/SecurityInterface.h"

class Message {
public:
	Message();
	Message(int message_id, string sess_id, string send_user, string send_dev_id, string group_name, string msg_data);
	virtual ~Message();
	std::string encode(SecurityInterface*,PublicKey) const;
private:
	int message_id;
	std::string session_id;
	std::string sender_username;
	std::string sender_device_id;
	std::string group_name;
	std::string message_data;
};

#endif /* MESSAGE_H_ */
