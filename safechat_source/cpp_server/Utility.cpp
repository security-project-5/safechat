/*
 * utility.cpp
 *
 *  Created on: Feb 10, 2016
 *      Author: daniel
 */

#include "Utility.h"

std::string Utility::getToken(istream* in){
	std::string str;
	getline(*in, str);
	return str;
}

unsigned int Utility::getTokenAsUInt(istream* in){
	std::string str = getToken(in);
	return strToUint(str);
}

unsigned int Utility::strToUint(std::string str){
	stringstream ss;
	ss << str;
	unsigned int uint;
	ss >> uint;

	return uint;
}



