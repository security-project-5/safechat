/*
 * PilgramServerSocket.cpp
 *
 *  Created on: Mar 28, 2016
 *      Author: daniel
 */

#include "PilgramServerSocket.h"

PilgramServerSocket::PilgramServerSocket(int port) : TCPServerSocket(port){
	memory = new DB();
	security = new RealSecurity();
	maxDesc = sockDesc;
	FD_ZERO(&readset);
	FD_SET(sockDesc, &readset);
	selectCount = 0;
}

void PilgramServerSocket::select() throw (SocketException){
	selectCount++;
	// Prevents "MySQL has gone away" error
	if(selectCount > 2870) {
		memory = new DB();
		selectCount = 0;
	}
	fd_set tempset;
	timeval tv;
	tv.tv_sec = 0;
	// 10 seconds
	int seconds = 10;
	tv.tv_usec = 1000 * 1000 * seconds;
	memcpy(&tempset, &readset, sizeof(tempset));
	int result = ::select(maxDesc+1, &tempset, NULL, NULL, &tv);
	if(result == 0) {
		//timeout
		cout << "select() timeout" << endl;
		return;
	}
	else if(result < 0) {
		throw SocketException("Select failed (select())", true);
	}
	else {
		if(FD_ISSET(sockDesc, &tempset)) {
			TCPSocket* clientSock = accept();
			cout << "Accepted client: " << clientSock->getForeignAddress() << ":" << clientSock->getForeignPort() <<  " with fd of " << clientSock->sockDesc << endl;
			FD_SET(clientSock->sockDesc, &readset);
			maxDesc = (maxDesc < clientSock->sockDesc)? clientSock->sockDesc : maxDesc;
			FD_CLR(sockDesc, &tempset);
		}

		for(int i = 0; i < maxDesc+1; i++) {
			if(FD_ISSET(i, &tempset)) {
				cout << "Client fd: " << i << " ready to read." << endl;
				TCPSocket clientSocket(i);
				PilgramClient client(&clientSocket, memory, security);
				cout << "retrieve client from map with fd: " << client.clientSocket->sockDesc << endl;
				bool finished = !client.receiveMore();
				if(finished) {
					cout << "Finished" << endl;
					clientSocket.close();
					FD_CLR(i, &readset);
				}
			}
		}
	}
}
