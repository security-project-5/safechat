/*
 * DeviceInfo.cpp
 *
 *  Created on: Mar 21, 2016
 *      Author: daniel
 */

#include "DeviceInfo.h"

DeviceInfo::DeviceInfo() {
	// TODO Auto-generated constructor stub
}

DeviceInfo::DeviceInfo(PublicKey key, std::string id) {
	this->key = key;
	this->id = id;
}

DeviceInfo::~DeviceInfo() {
}

std::string DeviceInfo::encode() const {
	std::string enc = "";
	enc += key._64_byte_array + NEW_LINE;
	enc += id + NEW_LINE;
	return enc;
}

