package gui;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import client.Friend;
import client.Group;
import client.Memory;
import client.Message;
import client.PiLGramException;
import client.SecurityMessenger;
import client.Message.MessageType;
import java.awt.Dimension;
import java.awt.CardLayout;
import javax.swing.JSplitPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Panel;
import java.awt.TextArea;

public class SMGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private JPanel contentPane;
	private HashMap<Group, JList<Message>> messagePages = new HashMap<>();
	
	private DefaultListModel<Group> groupListModel = new DefaultListModel<>();
	private JList<Group> groupList = new JList<>(groupListModel);
	private static boolean loggedIn = false;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
	        public void run() {
	            try {
	            	if(loggedIn) Memory.writeMemory();
				} catch (IOException | PiLGramException e) {
					e.printStackTrace();
				}
	        }
	    }, "Shutdown-thread"));
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SMGUI frame = new SMGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws PiLGramException 
	 */
	public SMGUI() throws PiLGramException {
		SecurityMessenger.create();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100,100,450,300);
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		contentPane = new JPanel(new CardLayout(0, 0));
		contentPane.setBorder(new EmptyBorder(5,5,5,5));
		setContentPane(contentPane);
		
		// CardLayout, contentPane, switches between login page and messenger page
		CardLayout cardLayout = new CardLayout(0, 0);
		contentPane.setLayout(cardLayout);
		
		// loginPanel is login page
			JPanel loginPanel = new JPanel();
			contentPane.add(loginPanel, "name_118460607173898");
			GridBagLayout gbl_loginPanel = new GridBagLayout();
			gbl_loginPanel.columnWidths = new int[]{91, 77, 114, 76, 4, 0};
			gbl_loginPanel.rowHeights = new int[]{19, 15, 25, 0, 0, 0, 0, 0, 0, 0, 0};
			gbl_loginPanel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_loginPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			loginPanel.setLayout(gbl_loginPanel);
			
			JLabel txtpnLoginPage = new JLabel();
			GridBagConstraints gbc_txtpnLoginPage = new GridBagConstraints();
			gbc_txtpnLoginPage.anchor = GridBagConstraints.WEST;
			gbc_txtpnLoginPage.insets = new Insets(0, 0, 5, 5);
			gbc_txtpnLoginPage.gridx = 2;
			gbc_txtpnLoginPage.gridy = 1;
			loginPanel.add(txtpnLoginPage, gbc_txtpnLoginPage);
			txtpnLoginPage.setText("Please Login");
			
			JLabel txtpnUsername = new JLabel();
			GridBagConstraints gbc_txtpnUsername = new GridBagConstraints();
			gbc_txtpnUsername.anchor = GridBagConstraints.WEST;
			gbc_txtpnUsername.insets = new Insets(0, 0, 5, 5);
			gbc_txtpnUsername.gridx = 1;
			gbc_txtpnUsername.gridy = 4;
			loginPanel.add(txtpnUsername, gbc_txtpnUsername);
			txtpnUsername.setText("Username:");
			
			JTextField usernameField = new JTextField();
			GridBagConstraints gbc_usernameField = new GridBagConstraints();
			gbc_usernameField.fill = GridBagConstraints.HORIZONTAL;
			gbc_usernameField.insets = new Insets(0, 0, 5, 5);
			gbc_usernameField.gridx = 2;
			gbc_usernameField.gridy = 4;
			loginPanel.add(usernameField, gbc_usernameField);
			usernameField.setColumns(10);
			
			JLabel txtpnPassword = new JLabel();
			GridBagConstraints gbc_txtpnPassword = new GridBagConstraints();
			gbc_txtpnPassword.anchor = GridBagConstraints.WEST;
			gbc_txtpnPassword.insets = new Insets(0, 0, 5, 5);
			gbc_txtpnPassword.gridx = 1;
			gbc_txtpnPassword.gridy = 5;
			loginPanel.add(txtpnPassword, gbc_txtpnPassword);
			txtpnPassword.setText("Password:");
			
			JPasswordField passwordField = new JPasswordField();
			GridBagConstraints gbc_passwordField = new GridBagConstraints();
			gbc_passwordField.fill = GridBagConstraints.HORIZONTAL;
			gbc_passwordField.insets = new Insets(0, 0, 5, 5);
			gbc_passwordField.gridx = 2;
			gbc_passwordField.gridy = 5;
			loginPanel.add(passwordField, gbc_passwordField);
			
			JButton logInButton = new JButton("Submit");
			GridBagConstraints gbc_logInButton = new GridBagConstraints();
			gbc_logInButton.anchor = GridBagConstraints.NORTH;
			gbc_logInButton.insets = new Insets(0, 0, 5, 5);
			gbc_logInButton.gridx = 2;
			gbc_logInButton.gridy = 6;
			loginPanel.add(logInButton, gbc_logInButton);
		
		// viewPanel is messenger page
		JPanel viewPanel = new JPanel();
		viewPanel.setLayout(new BorderLayout(0, 0));
		
			contentPane.add(viewPanel, "name_132965791943628");
			
			// writeMessagePanel appears below friend list and message list
			Panel writeMessagePanel = new Panel();
			viewPanel.add(writeMessagePanel, BorderLayout.SOUTH);
			writeMessagePanel.setLayout(new BorderLayout(0, 0));
			
			Button submitMessageButton = new Button("Send Message");
			
			JPanel panel = new JPanel();
			writeMessagePanel.add(panel, BorderLayout.WEST);
			panel.setLayout(new BorderLayout(10, 10));
			JButton btnLogout = new JButton("Log Out");
			JLabel whoAmI = new JLabel("Logged in as");
			whoAmI.setBorder(new EmptyBorder(10,10,10,10));
			panel.add(whoAmI, BorderLayout.NORTH);
					
			panel.add(btnLogout, BorderLayout.SOUTH);
			TextArea messageTextArea = new TextArea();
			messageTextArea.setBounds(0, 0, JFrame.MAXIMIZED_HORIZ-submitMessageButton.getWidth(), JFrame.MAXIMIZED_VERT);
			writeMessagePanel.add(messageTextArea, BorderLayout.CENTER);
			writeMessagePanel.add(submitMessageButton, BorderLayout.EAST);
			
	
			JToolBar toolBar = new JToolBar();
			Button refreshInboxButton = new Button("Refresh Inbox");
			toolBar.add(refreshInboxButton);
			refreshInboxButton.setPreferredSize(new Dimension(150, 30));
			refreshInboxButton.setMaximumSize(new Dimension(150, 30));
			refreshInboxButton.setMinimumSize(new Dimension(150, 30));
			
			viewPanel.add(toolBar, BorderLayout.PAGE_START);
			
			Button newGroupMessageButton = new Button("Create Group Message");
			newGroupMessageButton.setPreferredSize(new Dimension(150, 30));
			newGroupMessageButton.setMaximumSize(new Dimension(150, 30));
			newGroupMessageButton.setMinimumSize(new Dimension(150, 30));
			toolBar.add(newGroupMessageButton);
			
			groupList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			groupList.setCellRenderer(new GroupPanelRenderer());
			JScrollPane friendScroll = new JScrollPane();
			friendScroll.setMinimumSize(new Dimension(300, JFrame.MAXIMIZED_VERT-toolBar.getHeight()));
			friendScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			
			JScrollPane messageScroll = new JScrollPane();
			messageScroll.setMinimumSize(new Dimension(700, 700));
			messageScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, friendScroll, messageScroll);
			viewPanel.add(splitPane, BorderLayout.CENTER);
			splitPane.setDividerSize(0);
			splitPane.setDividerLocation(-1);
			
			
			// Action listeners
			
			
			// When clicking on a friend in the friend list
			ListSelectionListener groupListSelectionListener = new ListSelectionListener() {
				// New friend selected action;
				@Override
				public void valueChanged(ListSelectionEvent e) {
					Group currentGroup = groupList.getSelectedValue();
					messageScroll.setViewportView(messagePages.get(currentGroup));
					currentGroup.markMessagesRead();
				}
				
			};
			
			
			newGroupMessageButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String usernames = JOptionPane.showInputDialog(contentPane, "Enter comma-delimited list of usernames to add to this group:", "Start Group Message", JOptionPane.QUESTION_MESSAGE);
					String[] people = usernames.split(",");
					ArrayList<Friend> friends = new ArrayList<>();
					for(String p : people) {
						Friend f = Memory.getFriend(p);
						if(f != null) {
							friends.add(f);
						}
						else {
							try {
								SecurityMessenger.addFriend(p);
								f = Memory.getFriend(p);
								friends.add(f);
							} catch (PiLGramException e1) {
								JOptionPane.showMessageDialog(contentPane, "User: " + p + " does not exist. Can't create group.", "Create Group Error", JOptionPane.ERROR_MESSAGE);
								return;
							}
						}
					}
					friends.add(Memory.getFriend(Memory.getUser().getUsername()));
					String groupName = JOptionPane.showInputDialog(contentPane, "Enter group name:", "Start Group Message", JOptionPane.QUESTION_MESSAGE);
					if(groupName.length() <= 50) {
						Group g = new Group(friends, groupName);
						Memory.getUser().addGroup(g);
						groupListModel.addElement(g);
						DefaultListModel<Message> aMessageListModel = new DefaultListModel<>();
						JList<Message> messagesList = new JList<>(aMessageListModel);
						messagesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
						messagesList.setCellRenderer(new MessagePanelRenderer());
						messagesList.setBorder(new EmptyBorder(10, 10, 10, 10));
						messagePages.put(g, messagesList);
						groupList.setSelectedValue(g, true);
						messageScroll.setViewportView(messagesList);
					}
					else JOptionPane.showMessageDialog(contentPane, "Group name is too long. Limit is 50 characters. Can't create group.", "Create Group Error", JOptionPane.ERROR_MESSAGE);
				}
			});
			
			// When Refresh Inbox button is clicked
			refreshInboxButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						SecurityMessenger.refreshInbox();
						for(Group g : Memory.getUser().getGroups()) {
							if(g.hasNewMessages()) {
								ArrayList<Message> newMessages = g.getNewMessages();
								JList<Message> list = messagePages.get(g);
								if(list == null) {
									groupListModel.addElement(g);
									DefaultListModel<Message> aMessageListModel = new DefaultListModel<>();
									list = new JList<>(aMessageListModel);
									list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
									list.setCellRenderer(new MessagePanelRenderer());
									list.setBorder(new EmptyBorder(10, 10, 10, 10));
									messagePages.put(g, list);
									list = messagePages.get(g);
									int index = groupListModel.indexOf(g, 0);
									groupList.setSelectedIndex(index);
								}
								DefaultListModel<Message> listModel = (DefaultListModel<Message>) list.getModel();
								for(Message m : newMessages) {
									listModel.addElement(m);
								}
							}
						}
						if(!Memory.getUser().getFriends().isEmpty()) {
							groupList.getSelectedValue().markMessagesRead();
						}
						groupList.repaint();
					} catch(PiLGramException exc) {
						JOptionPane.showMessageDialog(contentPane, exc.getMessage(), "Receive Messages Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			
			
			// When clicking the Send Message button
			submitMessageButton.addActionListener(new ActionListener() {
				// Submit new message action
				public void actionPerformed(ActionEvent e) {
					Group g = groupList.getSelectedValue();
					System.out.println("Sending message to group: " + g.getName());
					if(!messageTextArea.getText().isEmpty()) {
						Message newMessage = new Message(new Date().getTime(), MessageType.sent, messageTextArea.getText(), Memory.getUser().getUsername());
						//refreshInboxButton.getActionListeners()[0].actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
						g.addMessage(newMessage);
						g.markMessagesRead();
						try {
							SecurityMessenger.sendMessage(g, messageTextArea.getText());
							JList<Message> messages = messagePages.get(g);
							DefaultListModel<Message> listModel = (DefaultListModel<Message>) messages.getModel();
							listModel.addElement(newMessage);
						} catch(PiLGramException exc) {
							JOptionPane.showMessageDialog(contentPane, exc.getMessage(), "Send Message Error", JOptionPane.ERROR_MESSAGE);
						}
						messageTextArea.setText("");
					}
					else {
						JOptionPane.showMessageDialog(contentPane, "You can't send an empty message!", "Error Sending Message", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
						
			// When clicking the Logout button
			btnLogout.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cardLayout.next(contentPane);
					try {
		            	if(loggedIn) Memory.writeMemory();
		            	groupList.removeListSelectionListener(groupListSelectionListener);
		            	groupListModel.clear();
		            	messagePages.clear();
					} catch (IOException | PiLGramException exc) {
						exc.printStackTrace();
					}
					loggedIn = false;
				}
			});
			
			
			// When clicking the Submit button to login
			logInButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						SecurityMessenger.create();
					} catch (PiLGramException e1) {
						JOptionPane.showMessageDialog(contentPane, "Can't find the server.", "Login Error", JOptionPane.ERROR_MESSAGE);
						return;
					}
					try {
						SecurityMessenger.validateUser(usernameField.getText(), new String(passwordField.getPassword()));
					} catch(IOException | PiLGramException e2) {
						JOptionPane.showMessageDialog(contentPane, "Login failed: " + e2.getMessage(), "Login Error", JOptionPane.ERROR_MESSAGE);
						return;
					}
					loggedIn = true;
					whoAmI.setText("Logged in as " + Memory.getUser().getUsername());
					ArrayList<Group> groups = Memory.getUser().getGroups();
					groupList = new JList<>(groupListModel);
					groupList.addListSelectionListener(groupListSelectionListener);
					groupList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
					groupList.setCellRenderer(new GroupPanelRenderer());
					friendScroll.setViewportView(groupList);
					for(Group g : groups) {
						groupListModel.addElement(g);
						DefaultListModel<Message> aMessageListModel = new DefaultListModel<>();
						for(Message m : g.getMessageHistory()) {
							aMessageListModel.addElement(m);
						}
						JList<Message> messagesList = new JList<>(aMessageListModel);
						messagesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
						messagesList.setCellRenderer(new MessagePanelRenderer());
						messagesList.setBorder(new EmptyBorder(10, 10, 10, 10));
						messagePages.put(g, messagesList);
					}
					if(!groups.isEmpty()) {
						groupList.setSelectedIndex(0);
						messageScroll.setViewportView(messagePages.get(groups.get(0)));
					}
					cardLayout.next(contentPane);
					try {
						SecurityMessenger.addFriend(Memory.getUser().getUsername());
					} catch(PiLGramException exc) {
						// It should be impossible to get here.
					}
				}
			});
	}
}
