package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import client.Memory;
import client.Message;
import client.Message.MessageType;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import java.awt.BorderLayout;

public class MessagePanelRenderer extends JPanel implements ListCellRenderer<Message> {
		private static final long serialVersionUID = 1L;
		private JLabel messageLabel = new JLabel("Example");
		JLabel timestampLabel = new JLabel("TimeStamp");
		JLabel fromLabel = new JLabel("Sender");
		JPanel container = new JPanel();
		
		
		public MessagePanelRenderer() {
			container.setLayout(new BorderLayout());
			container.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.black, 1, true), new EmptyBorder(0,3,3,3)));
			messageLabel.setHorizontalAlignment(SwingConstants.LEFT);
			messageLabel.setFont(new Font("Serif", Font.PLAIN, 20));
			container.add(messageLabel, BorderLayout.NORTH);
			timestampLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			timestampLabel.setFont(new Font("Serif", Font.PLAIN, 10));
			timestampLabel.setForeground(Color.LIGHT_GRAY);
			fromLabel.setForeground(Color.LIGHT_GRAY);
			fromLabel.setVerticalAlignment(SwingConstants.BOTTOM);
			fromLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			container.add(fromLabel, BorderLayout.EAST);
			container.add(timestampLabel, BorderLayout.SOUTH);
			this.add(container);
		}
		
		
		@Override
		public Component getListCellRendererComponent(JList<? extends Message> list, Message message, int index,
				boolean isSelected, boolean cellHasFocus) {
			if(message.getType().value() == MessageType.sent.value()) {
				this.setBorder(new EmptyBorder(0, 300, 0, 0));
				container.setBackground(Color.CYAN);
			}
			else {
				this.setBorder(new EmptyBorder(0, 0, 0, 300));
				container.setBackground(Color.YELLOW);
			}
			fromLabel.setText(message.getSender());
			timestampLabel.setText(new Date(message.getTimestamp()).toString());
			messageLabel.setText(toMultiLine(message.getMessage()));
			setBackground(list.getBackground());
			return this;
		}
		

		private String toMultiLine(String orig) {
			for(int i = 0; i < orig.length(); i++) {
				if(i%30 == 0) {
					orig = orig.substring(0, i) + "\n" + orig.substring(i);
					i++;
				}
			}
			return "<html>" + orig.replaceAll("\n", "<br>");
		}
	}