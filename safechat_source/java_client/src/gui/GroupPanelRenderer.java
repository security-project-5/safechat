package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import client.Friend;
import client.Group;
import client.Memory;
import client.Message.MessageType;

public class GroupPanelRenderer extends JPanel implements ListCellRenderer<Group> {

	private static final long serialVersionUID = 1L;
	private JLabel groupLabel = new JLabel("Group Name");
	private JLabel membersLabel = new JLabel("Members:");
	private JLabel hasNewMessagesLabel = new JLabel("New Message!");
	private JPanel container = new JPanel();
	
	
	public GroupPanelRenderer() {
		container.setLayout(new BorderLayout(10,10));
		groupLabel.setHorizontalAlignment(SwingConstants.LEFT);
		groupLabel.setFont(new Font("Serif", Font.PLAIN, 20));
		container.add(groupLabel, BorderLayout.NORTH);
		membersLabel.setHorizontalAlignment(SwingConstants.LEFT);
		membersLabel.setFont(new Font("Serif", Font.PLAIN, 15));
		membersLabel.setForeground(Color.DARK_GRAY);
		container.add(membersLabel, BorderLayout.WEST);
		hasNewMessagesLabel.setForeground(Color.BLACK);
		hasNewMessagesLabel.setHorizontalAlignment(SwingConstants.LEFT);
		hasNewMessagesLabel.setVisible(false);
		container.add(hasNewMessagesLabel, BorderLayout.EAST);
		this.add(container);
	}
	
	@Override
	public Component getListCellRendererComponent(JList<? extends Group> list, Group g, int index,
			boolean isSelected, boolean cellHasFocus) {
		if(g.hasNewMessages()) {
			hasNewMessagesLabel.setVisible(true);
		}
		else {
			hasNewMessagesLabel.setVisible(false);
		}
		groupLabel.setText(g.getName());
		String memberList = "";
		ArrayList<Friend> participants = g.getParticipants();
		for(int i = 0; i < participants.size()-1; i++) {
			memberList += participants.get(i).getUsername();
			if(i != participants.size()-2) memberList += ",";
		}
		membersLabel.setText("Members: " + memberList);
		if(isSelected) {
			setBackground(list.getSelectionBackground());
		}
		else {
			setBackground(list.getBackground());
		}
		return this;
	}

}
