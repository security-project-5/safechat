package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import client.Friend;
import client.Memory;
import client.Message.MessageType;

public class FriendPanelRenderer extends JPanel implements ListCellRenderer<Friend> {

	private static final long serialVersionUID = 1L;
	private JLabel friendUsernameLabel = new JLabel("Friend UserName");
	private JLabel friendNameLabel = new JLabel("Friend FullName");
	private JLabel phoneNumLabel = new JLabel("1234567890");
	private JLabel hasNewMessagesLabel = new JLabel("New Message!");
	private JPanel container = new JPanel();
	
	
	public FriendPanelRenderer() {
		container.setLayout(new BorderLayout(10,10));
		friendUsernameLabel.setHorizontalAlignment(SwingConstants.LEFT);
		friendUsernameLabel.setFont(new Font("Serif", Font.PLAIN, 20));
		container.add(friendUsernameLabel, BorderLayout.NORTH);
		friendNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
		friendNameLabel.setFont(new Font("Serif", Font.PLAIN, 10));
		friendNameLabel.setForeground(Color.LIGHT_GRAY);
		container.add(friendNameLabel, BorderLayout.WEST);
		phoneNumLabel.setForeground(Color.LIGHT_GRAY);
		phoneNumLabel.setHorizontalAlignment(SwingConstants.LEFT);
		phoneNumLabel.setFont(new Font("Serif", Font.PLAIN, 10));
		container.add(phoneNumLabel, BorderLayout.SOUTH);
		hasNewMessagesLabel.setForeground(Color.BLACK);
		hasNewMessagesLabel.setHorizontalAlignment(SwingConstants.LEFT);
		hasNewMessagesLabel.setVisible(false);
		container.add(hasNewMessagesLabel, BorderLayout.EAST);
		this.add(container);
	}
	
	@Override
	public Component getListCellRendererComponent(JList<? extends Friend> list, Friend f, int index,
			boolean isSelected, boolean cellHasFocus) {
		if(f.hasNewMessages()) {
			hasNewMessagesLabel.setVisible(true);
		}
		else {
			hasNewMessagesLabel.setVisible(false);
		}
		friendUsernameLabel.setText(f.getUsername());
		friendNameLabel.setText(f.getFirstname() + " " + f.getLastname());
		phoneNumLabel.setText(f.getPhoneNumber());
		if(isSelected) {
			setBackground(list.getSelectionBackground());
		}
		else {
			setBackground(list.getBackground());
		}
		return this;
	}

}
