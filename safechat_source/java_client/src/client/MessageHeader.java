package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.ParseException;
import java.util.Date;

public class MessageHeader implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum MessageHeaderType {
		loginRequest(0),
		loginResponse(1),
		addUserRequest(2),
		userAddResponse(3),
		establishSessionKey(4),
		messageSend(5),
		getMessagesRequest(6),
		messagesResponse(7);
		
		private final int value;
		
		private MessageHeaderType(int value) {
			this.value = value;
		}
		
		public int value() {
			return value;
		}
		
		// Ugly, but only way I can find to do this. Or could create a map of int->MessageType
		public static MessageHeaderType create(int value) throws PiLGramException {
			if(MessageHeaderType.loginRequest.value() == value) {
				return MessageHeaderType.loginRequest;
			} else if(MessageHeaderType.loginResponse.value() == value) {
				return MessageHeaderType.loginResponse;
			} else if(MessageHeaderType.addUserRequest.value() == value) {
				return MessageHeaderType.addUserRequest;
			} else if(MessageHeaderType.userAddResponse.value() == value) {
				return MessageHeaderType.userAddResponse;
			} else if(MessageHeaderType.establishSessionKey.value() == value) {
				return MessageHeaderType.establishSessionKey;
			} else if(MessageHeaderType.messageSend.value() == value) {
				return MessageHeaderType.messageSend;
			} else if(MessageHeaderType.getMessagesRequest.value() == value) {
				return MessageHeaderType.getMessagesRequest;
			} else if(MessageHeaderType.messagesResponse.value() == value) {
				return MessageHeaderType.messagesResponse;
			}
			else throw new PiLGramException("Invalid Message Type");
		}
	}
	
	private final String DELIM = "\n";
	
	private final long messageID;
	private MessageHeaderType messageType;
	private BigInteger timestamp;
	
	public MessageHeader(MessageHeaderType messageType) {
		this.messageType = messageType;
		this.messageID = new SecureRandom().nextInt();
		this.timestamp = new BigInteger(Long.toUnsignedString(new Date().getTime()));
	}
	
	private MessageHeader(MessageHeaderType messageType, long messageID, String timestamp) {
		this.messageType = messageType;
		this.messageID = messageID;
		this.timestamp = new BigInteger(timestamp);
	}
	
	public String toString() {
		return this.messageID + DELIM + this.messageType.value() + DELIM + this.timestamp + DELIM;
	}
	
	public static MessageHeader readHeader(BufferedReader in) throws IOException, PiLGramException, NumberFormatException, ParseException {
		String id = in.readLine();
		String type = in.readLine();
		String timestamp = in.readLine();
		MessageHeaderType m = MessageHeaderType.create(Integer.parseInt(type));
		return new MessageHeader(m, Long.parseLong(id), timestamp);
	}

	public long getMessageID() {
		return messageID;
	}

	public MessageHeaderType getMessageType() {
		return messageType;
	}
	
	public String getTimestamp() {
		return timestamp.toString();
	}
	
}
