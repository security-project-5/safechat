package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.UUID;

public class Message implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final BigInteger renewalInterval = new BigInteger(Long.toUnsignedString(1000*60*60));

	public enum MessageType {
		sent(0),
		received(1);
		
		private final int value;
		
		private MessageType(int value) {
			this.value = value;
		}
		
		public int value() {
			return value;
		}	
	}
	private static final String DELIM = "\n";
	
	private long timestamp;
	private MessageType type;
	private String message;
	private String senderName;
	
	public Message(long timestamp, MessageType type, String message, String senderName) {
		this.timestamp = timestamp;
		this.type = type;
		this.message = message;
		this.senderName = senderName;
	}

	public MessageType getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	
	public String getSender() {
		return senderName;
	}
	
	
	// returns if done reading messages
	public static boolean readMessage(BufferedReader in, String messageTimestamp) throws IOException, PiLGramException {
		String temp = "" + (char)in.read();
		if(temp.equals(DELIM)) {
			System.out.println("No more messages");
			return true;
		}
		else {
			String exceptionMessage = "";
			String message_header = "";
			String part = temp + in.readLine();
			while(!part.isEmpty()) {
				message_header += CryptUtil.toMe(part);
				part = in.readLine();
			}
			System.out.println("message_header<" + message_header + ">");
			String message_header_sig = in.readLine();
			System.out.println("message_header_sig<" + message_header_sig + ">");
			if(!CryptUtil.verify(message_header, message_header_sig)) {
				exceptionMessage = "Message header signature from Pilgram Server does not match. Someone could be trying to impersonate the Pilgram Server.";
			}
			BufferedReader header_reader = new BufferedReader(new StringReader(message_header));
			String session_id = header_reader.readLine();
			String sender_username = header_reader.readLine();
			String sender_device_id = header_reader.readLine();
			String adv_group_name = header_reader.readLine();
			
			Device d = Memory.getDevice(UUID.fromString(sender_device_id));
			Friend f = Memory.getFriend(sender_username);
			Group g = Memory.getGroup(adv_group_name);
			String enc_message_data = in.readLine();
			String message_data_sig = in.readLine();
			if(f == null) {
				exceptionMessage = "User: " + sender_username + " is attempting to communicate with you. Add them as a friend to communicate with them.";				
			}
			else if(g == null) {
				exceptionMessage = "User: " + sender_username + " has sent you a group message for a group that you don't know about!";
			}
			else {
				System.out.println("Sender friend:" + f.getUsername());
				SessionKey messageKey = g.findSessionKey(UUID.fromString(session_id));
				if(messageKey == null) {
					exceptionMessage = "Message's session key id does not exist on client.";
				}
				else {
					if(messageKey.isValidForMessageTimeStamp(messageTimestamp)) {
						messageKey.renew(messageTimestamp);
						g.removeExpiredKeysAtTime(messageTimestamp);
						System.out.println("Enc message data<" + enc_message_data + ">");
						String message_data = CryptUtil.symDecrypt(g.getCurrentSessionKey(), enc_message_data);
						System.out.println("message data<" + message_data + ">");
						System.out.println("message data sig<" + message_data_sig + ">");
						if(!CryptUtil.verifyFrom(d, message_data, message_data_sig)) {
							exceptionMessage = "Message signature from user: " + f.getUsername() + " does not match. Someone may be trying to impersonate user: " + f.getUsername();
						}
						BufferedReader message_reader = new BufferedReader(new StringReader(message_data));
						long timestamp = Long.parseLong(message_reader.readLine());
						String real_group_name = message_reader.readLine();
						
						String message = message_reader.readLine();
						if(!adv_group_name.equals(real_group_name)) {
							exceptionMessage = "Message originally intended for a different user. Someone may be trying to impersonate user: " + f.getUsername();
						}
						else {
							g.addMessage(new Message(timestamp, MessageType.received, message, f.getUsername()));
						}
					}
					else {
						exceptionMessage = "Session key of a received message was expired before being sent by user:" + f.getUsername();
					}
				}
			}
			if(!exceptionMessage.isEmpty()) {
				throw new PiLGramException(exceptionMessage);
			}
			return false;
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Message) {
			Message other = (Message) obj;
			return this.timestamp == other.getTimestamp() && this.type.value == other.type.value &&
					this.senderName.equals(other.senderName) && this.message.equals(other.message);
		}
		return false;
	}
	
}
