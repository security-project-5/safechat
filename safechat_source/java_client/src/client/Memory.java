package client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.UUID;

public class Memory {
	private static String magicString = "PILGRAM12345";
	private static User user;
	
	public static void createUser(String username, String password) throws PiLGramException {
		user = new User(username, CryptUtil.saltedHash(password, username));
	}
	
	public static boolean memoryExists() throws PiLGramException {
		return new File(user.getStorageFileName()).exists();
	}
	
	public static void createMemory(String password) throws IOException, PiLGramException {
		CryptUtil.generateKeyPair(password);
		new File(user.getStorageFileName()).createNewFile();
	}
	
	public static boolean eraseMemory() throws IOException, PiLGramException {
		File erase = new File(user.getStorageFileName());
		File erase2 = new File(user.getPublicKeyFileName());
		File erase3 = new File(user.getPrivateKeyFileName());
		File erase4 = new File(user.getLockIVFileName());

		boolean one = erase.delete();
		boolean two = erase2.delete();
		boolean three = erase3.delete();
		boolean four = erase4.delete();
		return one && two && three && four;
	}
	
	public static void readMemory(String password) throws IOException, PiLGramException {
		CryptUtil.readKeys(password);
		byte[] decrypted = CryptUtil.unlockStorage();
		ByteArrayInputStream stream = new ByteArrayInputStream(decrypted);
		ObjectInputStream reader = new ObjectInputStream(stream);
		String checkMagicString = null;
		try {
			Object obj = reader.readObject();
			if(obj instanceof String) {
				checkMagicString = (String) obj;
			}
			else throw new PiLGramException("Did not successfully unlock memory.");
			if(magicString.equals(checkMagicString)) {
				obj = reader.readObject();
				if(obj instanceof User) {
					user = (User) obj;
				}
				else throw new PiLGramException("Did not successfully unlock memory. Magic string does not match.");
			}
			else throw new PiLGramException("Did not successfully unlock memory.");
		} catch (ClassNotFoundException e) {
			throw new PiLGramException("Did not successfully unlock memory.", e.getCause());
		}
	}
	
	public static void writeMemory() throws IOException, PiLGramException {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		ObjectOutputStream writer = new ObjectOutputStream(stream);
		writer.writeObject(magicString);
		writer.writeObject(user);
		writer.flush();
		writer.close();
		byte[] encrypted = CryptUtil.lockStorage(stream.toByteArray());
		Files.write(Paths.get(Memory.getUser().getStorageFileName()), encrypted);
	}
	
	public static User getUser() {
		return user;
	}
	
	public static Device getDevice(UUID deviceID) {
		for(Friend f : user.getFriends()) {
			for(Device d : f.getDevices()) {
				if(d.getDeviceID().equals(deviceID)) return d;
			}
		}
		return null;
	}
	
	public static Friend getFriend(String friendUsername) {
		for(Friend f : user.getFriends()) {
			if(f.getUsername().equals(friendUsername)) return f;
		}
		return null;
	}
	
	public static Friend getFriendWithDevice(Device d) {
		for(Friend f : user.getFriends()) {
			for(Device dev : f.getDevices()) {
				if(dev.getDeviceID().equals(d)) return f;
			}
		}
		return null;
	}

	public static Group getGroup(String group_name) {
		for(Group group : user.getGroups()) {
			if(group.getName().equals(group_name)) return group;
		}
		return null;
	}
}
