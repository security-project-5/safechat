package client;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import client.MessageHeader.MessageHeaderType;

/**
 * MessageSender sends messages of all types to the server.
 * @author Ryan Cobb
 */
public class MessageSender {

	private PrintWriter out;
	
	private final String DELIM = "\n";
	private final String MSG_DELIM = "\r";
	private final String CHARSET = "US-ASCII";
	private final String SAFECHAT_VERSION = "SafeChat2.0";
	
	public MessageSender(OutputStream writeStream) {
		out = new PrintWriter(writeStream, true);
	}
	
	public void sendLoginRequest() throws PiLGramException, IOException {
		MessageHeader header = new MessageHeader(MessageHeaderType.loginRequest);
		byte[] pubKeyBytes = Files.readAllBytes(Paths.get(Memory.getUser().getPublicKeyFileName()));
		String pubKey = new String(CryptUtil.encode(pubKeyBytes), CHARSET);
		String signature = CryptUtil.sign(Memory.getUser().getUsername());
		String data = SAFECHAT_VERSION + DELIM + Memory.getUser().getUsername() + DELIM + Memory.getUser().getShPassword() + DELIM + Memory.getUser().getDeviceID() + DELIM + pubKey + DELIM + signature + DELIM;
		String payload = header + data;
		System.out.print("SENDING LOGIN WITH PAYLOAD<" + payload + ">");
		String message = CryptUtil.toServer(payload) + CryptUtil.hash(payload) + DELIM;
		this.sendMessage(message);
	}
	
	public void sendAddUserRequest(String friendUsername) throws PiLGramException {
		MessageHeader header = new MessageHeader(MessageHeaderType.addUserRequest);
		String data = Memory.getUser().getUsername() + DELIM + Memory.getUser().getDeviceID() + DELIM + friendUsername + DELIM;
		
		Friend f = Memory.getFriend(friendUsername);
		if(f == null) {
			f = new Friend(friendUsername);
			Memory.getUser().addFriend(f);
		}
		
		String payload = header + data;
		System.out.println("SENDING ADD CONTACT WITH PAYLOAD:<" + payload + ">");
		String message = CryptUtil.toServer(payload) + CryptUtil.sign(payload) + DELIM;
		this.sendMessage(message);
	}

	public void sendGetMessagesRequest() throws PiLGramException {
		MessageHeader header = new MessageHeader(MessageHeaderType.getMessagesRequest);
		String data = Memory.getUser().getUsername() + DELIM + Memory.getUser().getDeviceID() + DELIM;
		String payload = header + data;
		System.out.println("SENDING CHECK INBOX WITH PAYLOAD:<" + payload + ">");
		String message = CryptUtil.toServer(payload) + CryptUtil.sign(payload) + DELIM;
		this.sendMessage(message);
	}
	
	public void sendMessage(Group recipient, String messageContents) throws PiLGramException {
		for(Friend f : recipient.getParticipants()) {
			if(!f.getUsername().equals(Memory.getUser().getUsername())) {
				MessageHeader header = new MessageHeader(MessageHeaderType.messageSend);
				String dataHeader = recipient.getCurrentSessionKey().getSessionID() + DELIM + Memory.getUser().getUsername() + DELIM + Memory.getUser().getDeviceID() + DELIM
								+ f.getUsername() + DELIM + recipient.getName() + DELIM;
				String server_payload = header + dataHeader;
				System.out.println("SENDING A MESSAGE WITH SERVER_PAYLOAD<" + server_payload + ">");
				String server_payload_enc = CryptUtil.toServer(server_payload);
				String server_payload_sig = CryptUtil.sign(server_payload);
				
				String message_payload = header.getTimestamp() + DELIM + recipient.getName() + DELIM + messageContents + DELIM;
				System.out.println("AND MESSAGE PAYLOAD<" + message_payload + ">");
				String message_payload_enc = CryptUtil.symEncrypt(recipient.getCurrentSessionKey(), message_payload);
				String message_payload_sig = CryptUtil.sign(message_payload);
				
				String message = server_payload_enc + server_payload_sig + DELIM + message_payload_enc + DELIM + message_payload_sig + DELIM;
				recipient.getCurrentSessionKey().renew(header.getTimestamp());
				this.sendMessage(message);
			}
		}
	}
	
	public void establishSessionKey(Group recipient) throws PiLGramException {
		SessionKey key = recipient.createSessionKey();
		String data_to_enc = "";
		for(Friend f : recipient.getParticipants()) {
			data_to_enc += f.getUsername() + DELIM;
		}
		// Mark end of list
		data_to_enc += DELIM;
		data_to_enc += recipient.getName() + DELIM;
		data_to_enc += new String(CryptUtil.encode(key.getKey().getEncoded()), Charset.forName(CHARSET)) + DELIM;
		data_to_enc += new String(CryptUtil.encode(key.getIV()), Charset.forName(CHARSET)) + DELIM;
		data_to_enc += key.getExpirationTimeStamp() + DELIM;
		for(Friend f  : recipient.getParticipants()) {
			if(!f.getUsername().equals(Memory.getUser().getUsername())) {
				MessageHeader header = new MessageHeader(MessageHeaderType.establishSessionKey);
				System.out.println("Particapant: " + f.getUsername());
				for(Device d : f.getDevices()) {
					System.out.println("Device: " + d.getDeviceID());
					String enc = CryptUtil.toDeviceFromMe(d, data_to_enc);
					String payload = header + key.getSessionID() + DELIM + Memory.getUser().getUsername() + DELIM + Memory.getUser().getDeviceID().toString() + DELIM + d.getDeviceID().toString() + DELIM + enc;
					System.out.println("SENDING ESTABLISH SESSION WITH PAYLOAD<" + payload + ">");
					String payload_enc = CryptUtil.toServer(payload);
					String payload_sig = CryptUtil.sign(payload);
					String message = payload_enc + payload_sig + DELIM;
					this.sendMessage(message);
				}
			}
		}
	}
	
	private void sendMessage(String contents) {
		out.print(contents + MSG_DELIM);
		out.flush();
	}

}
