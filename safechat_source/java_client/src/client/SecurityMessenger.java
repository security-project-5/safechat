/**
 * Author:		Ryan Cobb
 * Project:		PiLGram
 * 
 * Secure Messenger Client
 */

package client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;


/**
 * SecurityMessenger is the driver for the Client
 * 
 * @author Ryan Cobb
 */
public class SecurityMessenger {
	
	private static final String CONFIG_FILE = "safechat.conf";
	
	// Socket communication
	private static Socket socket;
	private static MessageSender sender;
	private static MessageReceiver receiver;
	
	public static void create() throws PiLGramException {
		try {
			// Open server communication
			BufferedReader reader = new BufferedReader(new FileReader(new File(CONFIG_FILE)));
			String serverName = reader.readLine();
			String portNum = reader.readLine();
			reader.close();
			socket = new Socket(serverName, Integer.parseInt(portNum));
			sender = new MessageSender(socket.getOutputStream());
			receiver = new MessageReceiver(socket.getInputStream());
		} catch (IOException e) {
			throw new PiLGramException("Error establishing server connection");
		}
	}
	
	/**
	 * Client/Server communication for validating the user/password pair
	 * 
	 * @return boolean - true if user is validated, false otherwise
	 * @throws PiLGramException 
	 * @throws IOException 
	 */
	public static void validateUser(String username, String password) throws PiLGramException, IOException {
		Memory.createUser(username, password);
		
		boolean exists = Memory.memoryExists();
		if(!exists) {
			Memory.createMemory(password);
		}
		else {
			try {
				Memory.readMemory(password);
			} catch(PiLGramException e) {
				Memory.eraseMemory();
			}
		}
		sender.sendLoginRequest();
		try {
			receiver.receiveLoginResponse();
		} catch(PiLGramException e) {
			Memory.eraseMemory();
			throw e;
		}
	}
	
	public static void sendMessage(Group recipient, String message) throws PiLGramException{
		try {
			for(Friend f : recipient.getParticipants()) {
				if(!f.getUsername().equals(Memory.getUser().getUsername())) {
					sender.sendAddUserRequest(f.getUsername());
					receiver.receiveUserAddResponse();
				}
			}
			//recipient = Memory.getFriend(recipient.getUsername());
			if(!recipient.hasSessionKey()) {
				for(Friend f : recipient.getParticipants()) {
					ArrayList<Device> devices = f.getDevices();
					if(devices.isEmpty()) {
						throw new PiLGramException("User: " + f.getUsername() + " has no registered devices. Wait for them to login from a device and then try again.");
					}
				}
				sender.establishSessionKey(recipient);
			}
			sender.sendMessage(recipient, message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void refreshInbox() throws PiLGramException {
		try {
			sender.sendGetMessagesRequest();
			receiver.receiveMessages();
		} catch (IOException | PiLGramException e) {
			throw new PiLGramException(e.getMessage(), e.getCause());
		}
	}
	
	public static void addFriend(String username) throws PiLGramException {
		try {
			sender.sendAddUserRequest(username);
			receiver.receiveUserAddResponse();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
