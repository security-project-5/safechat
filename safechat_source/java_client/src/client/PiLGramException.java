package client;

public class PiLGramException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PiLGramException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public PiLGramException(String message) {
		super(message);
	}

}
