package client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class Group implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String groupName;
	private SessionKey sessionKey;
	private ArrayList<SessionKey> currentReceiveKeys = new ArrayList<>();
	
	private boolean hasNewMessages;
	private int newMessageCount;
	
	private ArrayList<Friend> participants = new ArrayList<>();
	private ArrayList<Message> messageHistory = new ArrayList<>();
	
	
	public Group(ArrayList<Friend> friends, String groupName) {
		this.participants = friends;
		this.groupName = groupName;
		this.hasNewMessages = false;
		newMessageCount = 0;
	}
	
	public void addMessage(Message m) {
		hasNewMessages = true;
		newMessageCount++;
		messageHistory.add(m);
	}
	
	public boolean hasNewMessages() {
		return hasNewMessages;
	}
	
	public void markMessagesRead() {
		hasNewMessages = false;
		newMessageCount = 0;
	}
	
	public SessionKey createSessionKey() {
		sessionKey = new SessionKey();
		return sessionKey;
	}
	
	public SessionKey getCurrentSessionKey() {
		return sessionKey;
	}
	
	public SessionKey findSessionKey(UUID sessionID) {
		if(sessionKey != null && sessionKey.getSessionID().equals(sessionID.toString())) {
			return sessionKey;
		}
		for(SessionKey sk : currentReceiveKeys) {
			if(sk.getSessionID().equals(sessionID.toString())) {
				return sk;
			}
		}
		return null;
	}
		
	public void addSessionKey(SessionKey s) {
		// if s is greater than sessionKey, then it is the new sessionKey to use for sent messages
		if(sessionKey == null ||  s.isValidForMessageTimeStamp(sessionKey.getExpirationTimeStamp())) {
			sessionKey = s;
		}
		currentReceiveKeys.add(s);
	}
	
	public void removeExpiredKeysAtTime(String timestamp) {
		for(int i = 0; i < currentReceiveKeys.size(); i++) {
			if(!currentReceiveKeys.get(i).isValidForMessageTimeStamp(timestamp)) {
				currentReceiveKeys.remove(i);
			}
		}
	}
	
	public boolean hasSessionKey() {
		// Session keys expire after an hour
		if(sessionKey != null && sessionKey.isValidForMessageTimeStamp(Long.toUnsignedString(new Date().getTime()))) {
			return true;
		}
		return false;
	}
	
	public ArrayList<Message> getMessageHistory() {
		return messageHistory;
	}
	
	public ArrayList<Message> getNewMessages() {
		ArrayList<Message> newMessages = new ArrayList<>();
		for(int i = messageHistory.size()-newMessageCount; i < messageHistory.size(); i++) {
			newMessages.add(messageHistory.get(i));
		}
		return newMessages;
	}
	
	public ArrayList<Friend> getParticipants() {
		return participants;
	}
	
	public String getName() {
		return groupName;
	}
	
}
