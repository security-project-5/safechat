package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.UUID;

public class Device implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String DELIM = "\n";
	private static final String CHARSET = "US-ASCII";
	private static final String CRYPT_ALGO = "RSA";
	
	private UUID deviceID;
	private PublicKey publicKey;
	
	public Device(UUID deviceID, PublicKey publicKey) throws IOException, PiLGramException {
		this.deviceID = deviceID;
		this.publicKey = publicKey;
	}

	public UUID getDeviceID() {
		return deviceID;
	}

	public PublicKey getPublicKey() {
		return publicKey;
	}
	
	
	public static Device readDevice(BufferedReader in) throws IOException, PiLGramException {
		String temp = "" + (char)in.read();
		if(temp.equals(DELIM)) {
			return null;
		}
		String publicKey = temp + in.readLine();
		String deviceID = in.readLine();
		System.out.println("WITH DEVICE<" + publicKey + DELIM + deviceID + ">");
		// Consume last newline
		in.readLine();
		KeyFactory fact;
		PublicKey pk = null;
		try {
			fact = KeyFactory.getInstance(CRYPT_ALGO);
			EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(CryptUtil.decode(publicKey.getBytes(CHARSET)));
			pk = fact.generatePublic(pubKeySpec);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
			throw new PiLGramException(e.getMessage(), e.getCause());
		}
		return new Device(UUID.fromString(deviceID), pk);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deviceID == null) ? 0 : deviceID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Device other = (Device) obj;
		if (deviceID == null) {
			if (other.deviceID != null)
				return false;
		} else if (!deviceID.equals(other.deviceID))
			return false;
		return true;
	}
}
