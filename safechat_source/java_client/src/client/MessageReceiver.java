package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.text.ParseException;

import client.MessageHeader.MessageHeaderType;

/**
 * MessageReceiver receives messages of all types from the server
 * @author Ryan Cobb
 */
public class MessageReceiver {
	
	
	private final String MSG_DELIM = "\r";
	private final String DELIM = "\n";
	
	private BufferedReader in;
	private MessageHeader currentHeader;
	
	public MessageReceiver(InputStream readStream) {
		in = new BufferedReader(new InputStreamReader(readStream));	
	}
	
	public void receiveLoginResponse() throws IOException, PiLGramException {
		BufferedReader payloadReader = receiveMessageHeaderFromServerOfType(MessageHeaderType.loginResponse);
		String response = payloadReader.readLine();
		System.out.println("RECEIVE LOGIN RESPONSE<" + response + ">");
		payloadReader.close();
		if(!response.equals("ACCEPT")) {
			throw new PiLGramException(response);
		}
	}
	
	public void receiveUserAddResponse() throws IOException, PiLGramException {
		BufferedReader payloadReader = receiveMessageHeaderFromServerOfType(MessageHeaderType.userAddResponse);
		String friendUsername = payloadReader.readLine();
		if("INVALID_USERNAME".equals(friendUsername)) {
			throw new PiLGramException("Friend does not exist.");
		}
		String firstname = payloadReader.readLine();
		String lastname = payloadReader.readLine();
		String phoneNumber = payloadReader.readLine();
		String email = payloadReader.readLine();
		System.out.println("RECEIVE USER ADD RESPONSE WITH FRIEND<" + friendUsername + ">");
		Friend f = Memory.getFriend(friendUsername);
		if(f != null) {
			f.setFirstname(firstname);
			f.setLastname(lastname);
			f.setPhoneNumber(phoneNumber);
			f.setEmail(email);
			boolean done = false;
			while(!done) {
				Device d = Device.readDevice(payloadReader);
				if(d != null) f.addDevice(d);
				else done = true;
			}
			payloadReader.close();
		}
		else throw new PiLGramException("Friend not requested by user.");
	}
	
	public void receiveMessages() throws IOException, PiLGramException {
		String exceptionMessage = "";
		BufferedReader payloadReader = null;
		try {
			payloadReader = receiveMessageHeaderFromServerOfType(MessageHeaderType.messagesResponse);
		} catch(PiLGramException e) {
			exceptionMessage = e.getMessage();
		}
		boolean done = false;
		while(!done) {
			try {
				done = SessionKey.readSessionKey(payloadReader);
			} catch(PiLGramException e) {
				exceptionMessage = e.getMessage();
			}
		}
		done = false;
		while(!done) {
			try {
				done = Message.readMessage(payloadReader, currentHeader.getTimestamp());
			}
			catch(PiLGramException e) {
				exceptionMessage = e.getMessage();
			}
		}
		if(!exceptionMessage.isEmpty()) {
			throw new PiLGramException(exceptionMessage);
		}
	}
	
	private BufferedReader receiveMessageHeaderFromServerOfType(MessageHeaderType type) throws PiLGramException, IOException {
		String full = grabMessage();
		BufferedReader inputReader = new BufferedReader(new StringReader(full));
		String payload = "";
		String part = inputReader.readLine();
		String replacement = part + DELIM;
		while(!part.equals("")) {
			payload += CryptUtil.toMe(part);
			part = inputReader.readLine();
			replacement += part + DELIM;
		}
		System.out.println("Payload<" + payload + ">");
		String sig = inputReader.readLine();
		replacement += sig + DELIM;
		BufferedReader payloadReader = new BufferedReader(new StringReader(payload));;
		if(!CryptUtil.verify(payload, sig)) {
			throw new PiLGramException("Could not verify signature of message. Corrupted message.");
		}
		try {
			currentHeader = MessageHeader.readHeader(payloadReader);
		} catch (NumberFormatException | ParseException e) {
			throw new PiLGramException("Error reading message header.", e.getCause());
		}
		if(currentHeader.getMessageType() != type) {
			throw new PiLGramException("Received invalid message type");
		}
		if(MessageHeaderType.messagesResponse.value() == type.value()){
			full = full.replace(replacement, "");
			payloadReader = new BufferedReader(new StringReader(full));
		}
		return payloadReader;
	}
	
	private String grabMessage() throws IOException {
		String message = "";
		String temp = "" + (char)in.read();
		while(!temp.equals(MSG_DELIM)) {
			message += temp;
			temp = "" + (char) in.read();
		}
		return message;
	}
	
}
