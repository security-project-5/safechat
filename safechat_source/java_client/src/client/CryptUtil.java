package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class CryptUtil {
	
	private static PrivateKey myPrivateKey;
	//TODO private
	public static PublicKey myPublicKey;
	private static SecretKey fileUnlocker;
	private static final String SIG_ALGO = "SHA256withRSA";
	private static Signature mySignature;
	private static final String CHARSET = "US-ASCII";
	private static final String CRYPT_ALGO = "RSA";
	private static final String FULL_CRYPT_ALGO = "RSA/ECB/PKCS1Padding";
	private static final String DELIM = "\n";
	private static final String SYM_ALGO = "AES/CBC/PKCS5Padding";
	private static final int SYM_KEY_SIZE = 128;
	private static final String HASH_ALGO = "SHA-256";
	private static final int KEY_SIZE = 2048;
	private static final int MAX_MSG_SIZE = (KEY_SIZE/8) - 11;
	private static PublicKey serverPublicKey;
	private static final String SERVER_PK_FILENAME = "serverPublicKey.der";
	private static String lockSalt;
	private static int pswdIterations = 65536;
	private static byte[] lockIV = new byte[SYM_KEY_SIZE/8];
	
	public static String saltedHash(String input, String salt) throws PiLGramException {
		return hash(input + salt);
	}
	
	public static String hash(String input) throws PiLGramException {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance(HASH_ALGO);
		} catch (NoSuchAlgorithmException e) {
			throw new PiLGramException(e.getMessage(), e.getCause());
		}
		md.update(input.getBytes(Charset.forName(CHARSET)));
		byte[] bytes = md.digest();
		return new String(encode(bytes), Charset.forName(CHARSET));

	}
	
	public static String symEncrypt(SessionKey key, String message) {
		try {
			final Cipher cipher = Cipher.getInstance(SYM_ALGO);
			SecretKeySpec skeySpec = new SecretKeySpec(key.getKey().getEncoded(), "AES");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(key.getIV()));
			byte[] byteOutput = cipher.doFinal(message.getBytes(CHARSET));
			String fin = new String(encode(byteOutput), CHARSET);
			return fin;
		} catch (InvalidKeyException | InvalidAlgorithmParameterException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static String symDecrypt(SessionKey key, String message) {
		try {
			final Cipher cipher = Cipher.getInstance(SYM_ALGO);
			SecretKeySpec skeySpec = new SecretKeySpec(key.getKey().getEncoded(), "AES");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(key.getIV()));
			byte[] byteInput = decode(message.getBytes(CHARSET));
			byte[] byteOutput = cipher.doFinal(byteInput);
			return new String(byteOutput, CHARSET);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static String toServer(String input) throws PiLGramException {
		return encryptWithKey(input, serverPublicKey);
	}
	
	public static String sign(String input) throws PiLGramException {
		try {
			mySignature.initSign(myPrivateKey);
			mySignature.update(input.getBytes(CHARSET));
			byte[] byteOutput = mySignature.sign();
			return new String(encode(byteOutput), CHARSET);
		} catch (SignatureException | UnsupportedEncodingException | InvalidKeyException e) {
			e.printStackTrace();
			throw new PiLGramException(e.getMessage(), e.getCause());
		}
	}
	
	public static boolean verify(String plain, String signed) throws PiLGramException {
		try {
			mySignature.initVerify(serverPublicKey);
			mySignature.update(plain.getBytes(CHARSET));
			return mySignature.verify(decode(signed.getBytes(CHARSET)));
		} catch (SignatureException | UnsupportedEncodingException | InvalidKeyException e) {
			e.printStackTrace();
			throw new PiLGramException(e.getMessage(), e.getCause());
		}
	}
	
	public static boolean verifyFrom(Device d, String plain, String signed) throws PiLGramException {
		try {
			mySignature.initVerify(d.getPublicKey());
			mySignature.update(plain.getBytes(CHARSET));
			return mySignature.verify(decode(signed.getBytes(CHARSET)));
		} catch (SignatureException | UnsupportedEncodingException | InvalidKeyException e) {
			e.printStackTrace();
			throw new PiLGramException(e.getMessage(), e.getCause());
		}
	}
	
	public static String toMe(String input) throws PiLGramException {
		return decryptWithKey(input, myPrivateKey);
	}
	
	
	public static String fromDeviceToMe(Device d, String input) throws PiLGramException {
		BufferedReader reader = new BufferedReader(new StringReader(input));
		String part, whole2 = null;
		try {
			part = reader.readLine();
			String whole = "";
			while(!part.isEmpty()) {
				whole += decryptWithKey(part, myPrivateKey);
				part = reader.readLine();
			}
			reader = new BufferedReader(new StringReader(whole));
			part = reader.readLine();
			whole2 = "";
			while(!part.isEmpty()) {
				whole2 += decryptWithKey(part, d.getPublicKey());
				part = reader.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return whole2;
	}
	
	public static String toDeviceFromMe(Device d, String input) throws PiLGramException {
		String signedByMe = encryptWithKey(input, myPrivateKey);
		String encryptedForFriend = encryptWithKey(signedByMe, d.getPublicKey());
		return encryptedForFriend;
	}
	
	private static String encryptWithKey(String input, Key key) throws PiLGramException {
		try {
			final Cipher cipher = Cipher.getInstance(FULL_CRYPT_ALGO);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			String output = "";
			int bytesRemaining = input.getBytes(CHARSET).length;
			int bytesRead = 0;
			while(bytesRemaining > 0) {
				if(bytesRemaining > MAX_MSG_SIZE) {
					byte[] byteOutput = cipher.doFinal(input.substring(bytesRead, bytesRead+MAX_MSG_SIZE).getBytes(CHARSET));
					byteOutput = encode(byteOutput);
					String suboutput = new String(byteOutput, CHARSET);
					output += suboutput + DELIM;
					bytesRemaining -= MAX_MSG_SIZE;
					bytesRead += MAX_MSG_SIZE;
				}
				else {
					String subinput = input.substring(bytesRead);
					byte[] byteOutput = cipher.doFinal(subinput.getBytes(CHARSET));
					byteOutput = encode(byteOutput);
					String suboutput = new String(byteOutput, CHARSET);
					output += suboutput + DELIM;
					bytesRemaining = 0;
				}
			}
			return output + DELIM;
		} catch (IllegalBlockSizeException | BadPaddingException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new PiLGramException(e.getMessage(), e.getCause());
		}

	}
	//TODO make this private
	private static String decryptWithKey(String input, Key key) throws PiLGramException {
		try {
			final Cipher cipher = Cipher.getInstance(FULL_CRYPT_ALGO);
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] byteDecoded = decode(input.getBytes(CHARSET));
			byte[] byteDecrypted = cipher.doFinal(byteDecoded);
			return new String(byteDecrypted, CHARSET);
		} catch (IllegalBlockSizeException | BadPaddingException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new PiLGramException(e.getMessage(), e.getCause());
		}
		
	}
	
	public static void generateKeyPair(String password) throws IOException, PiLGramException {
		try {
			lockSalt = Memory.getUser().getUsername();
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance(CRYPT_ALGO);
			keyGen.initialize(KEY_SIZE);
			KeyPair keyPair = keyGen.generateKeyPair();

			myPublicKey = keyPair.getPublic();
			myPrivateKey = keyPair.getPrivate();
			Files.write(Paths.get(Memory.getUser().getPublicKeyFileName()), myPublicKey.getEncoded());
			
			Files.write(Paths.get(Memory.getUser().getPrivateKeyFileName()), lockPrivateKey(password));
			readServerPublic();
			mySignature = Signature.getInstance(SIG_ALGO);
			
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new PiLGramException(e.getMessage(), e.getCause());
		}		
	}
	
	public static void readKeys(String password) throws IOException, PiLGramException {
		try {
			lockSalt = Memory.getUser().getUsername();
			KeyFactory fact = KeyFactory.getInstance(CRYPT_ALGO);
			byte[] encPrivKey = Files.readAllBytes(Paths.get(Memory.getUser().getPrivateKeyFileName()));
			byte[] privKey = unlockPrivateKey(encPrivKey, password);
			EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(privKey);
			EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(Files.readAllBytes(Paths.get(Memory.getUser().getPublicKeyFileName())));
			myPublicKey = fact.generatePublic(pubKeySpec);
			myPrivateKey = fact.generatePrivate(privKeySpec);
			readServerPublic();
			mySignature = Signature.getInstance(SIG_ALGO);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
			throw new PiLGramException(e.getMessage(), e.getCause());
		}
	}
	
	public static void readServerPublic() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
		EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(Files.readAllBytes(Paths.get(SERVER_PK_FILENAME)));
		KeyFactory fact = KeyFactory.getInstance(CRYPT_ALGO);
		serverPublicKey = fact.generatePublic(pubKeySpec);
	}
	
	private static byte[] lockPrivateKey(String password) throws PiLGramException {
		try {
	        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
	        PBEKeySpec spec = new PBEKeySpec(
	                password.toCharArray(),
	                lockSalt.getBytes(CHARSET), 
	                pswdIterations, 
	                SYM_KEY_SIZE
	                );
	 
	        fileUnlocker = factory.generateSecret(spec);
	        SecretKeySpec secret = new SecretKeySpec(fileUnlocker.getEncoded(), "AES");
	 
	        // Decrypt the message
	        Cipher cipher = Cipher.getInstance(SYM_ALGO);
	        new SecureRandom().nextBytes(lockIV);
			Files.write(Paths.get(Memory.getUser().getLockIVFileName()), lockIV);
	        cipher.init(Cipher.ENCRYPT_MODE, secret, new IvParameterSpec(lockIV));
	        return cipher.doFinal(myPrivateKey.getEncoded());
		}
		catch(IOException | NoSuchAlgorithmException | InvalidKeySpecException |
				NoSuchPaddingException | InvalidKeyException |
				InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
			throw new PiLGramException("Problem locking private key file.");
		}
	}
	
	private static byte[] unlockPrivateKey(byte[] encPrivKey, String password) throws PiLGramException {
		try {
	        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
	        PBEKeySpec spec = new PBEKeySpec(
	                password.toCharArray(), 
	                lockSalt.getBytes(CHARSET), 
	                pswdIterations, 
	                SYM_KEY_SIZE
	                );
	 
	        fileUnlocker = factory.generateSecret(spec);
	        SecretKeySpec secret = new SecretKeySpec(fileUnlocker.getEncoded(), "AES");
	 
	        // Decrypt the message
	        Cipher cipher = Cipher.getInstance(SYM_ALGO);
	        lockIV = Files.readAllBytes(Paths.get(Memory.getUser().getLockIVFileName()));
	        cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(lockIV));
	        return cipher.doFinal(encPrivKey);
		}
		catch(IOException | NoSuchAlgorithmException | InvalidKeySpecException |
				NoSuchPaddingException | InvalidKeyException |
				InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
			throw new PiLGramException("Problem unlocking private key file.");
		}
	}
	
	public static byte[] lockStorage(byte[] input) throws PiLGramException {
		try {
			Cipher cipher = Cipher.getInstance(SYM_ALGO);
			SecretKeySpec secret = new SecretKeySpec(fileUnlocker.getEncoded(), "AES");
			cipher.init(Cipher.ENCRYPT_MODE, secret, new IvParameterSpec(lockIV));
			return cipher.doFinal(input);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
			throw new PiLGramException("Problem locking storage file.");
		}
	}
	
	public static byte[] unlockStorage() throws PiLGramException {
		try {
			Cipher cipher = Cipher.getInstance(SYM_ALGO);
			SecretKeySpec secret = new SecretKeySpec(fileUnlocker.getEncoded(), "AES");
			cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(lockIV));
			return cipher.doFinal(Files.readAllBytes(Paths.get(Memory.getUser().getStorageFileName())));
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | IOException e) {
			e.printStackTrace();
			throw new PiLGramException("Problem unlocking storage file.");
		}
		
	}
	
	public static String encode(String input) {
		Base64.Encoder encoder = Base64.getEncoder();
		return encoder.encodeToString(input.getBytes(Charset.forName(CHARSET)));
	}
	
	public static String decode(String input) {
		Base64.Decoder decoder = Base64.getDecoder();
		return new String(decoder.decode(input), Charset.forName(CHARSET));
	}
	
	public static byte[] encode(byte[] input) {
		Base64.Encoder encoder = Base64.getEncoder();
		return encoder.encode(input);
	}
	
	public static byte[] decode(byte[] input) {
		Base64.Decoder decoder = Base64.getDecoder();
		return decoder.decode(input);
	}
	
	public static String filename(String input) {
		input = input.replace("/", "");
		input = input.replace(">", "");
		input = input.replace("<", "");
		input = input.replace("|", "");
		input = input.replace(":", "");
		input = input.replace("&", "");
		return input;
	}
}
