package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class SessionKey implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private final String ALGO = "AES";
	private final static String CHARSET = "US-ASCII";
	private final static String DELIM = "\n";
	private final int KEYSIZE = 128;
	public static final BigInteger renewalInterval = new BigInteger(Long.toUnsignedString(1000*60*60));
	private UUID sessionID;
	private BigInteger expirationTimeStamp;
	private SecretKey key;
	private byte[] iv = new byte[KEYSIZE/8];
	
	public SessionKey() {
		try {
			KeyGenerator keyGen = KeyGenerator.getInstance(ALGO);
			keyGen.init(KEYSIZE);
			key = keyGen.generateKey();
			expirationTimeStamp = renewalInterval.add(new BigInteger(Long.toUnsignedString(new Date().getTime())));
			System.out.println("Create session key with expiration: " + expirationTimeStamp);
			new SecureRandom().nextBytes(iv);
			sessionID = UUID.randomUUID();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public SessionKey(String encodedKey, byte[] iv, String timeStamp, UUID sessionID) {
		try {
			byte[] decodedKey = CryptUtil.decode(encodedKey.getBytes(CHARSET));
			key = new SecretKeySpec(decodedKey, 0, decodedKey.length, ALGO);
			this.iv = iv;
			this.sessionID = sessionID;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		this.expirationTimeStamp = new BigInteger(timeStamp);
	}
	
	// returns if done reading keys
	public static boolean readSessionKey(BufferedReader in) throws IOException, PiLGramException {
		String temp = "" + (char)in.read();
		if(temp.equals(DELIM)) {
			System.out.println("No More Session Keys");
			// no more session keys
			return true;
		}
		else {
			String exceptionMessage = "";
			String session_packet_payload = "";
			String part = temp + in.readLine();
			while(!part.isEmpty()) {
				session_packet_payload += CryptUtil.toMe(part);
				part = in.readLine();
			}
			System.out.println("session server packet payload<" + session_packet_payload + ">");
			String session_packet_payload_sig = in.readLine();
			if(!CryptUtil.verify(session_packet_payload, session_packet_payload_sig)) {
				exceptionMessage = "Could not verify signature of message. Corrupted message.";
			}
			BufferedReader session_packet_payload_reader = new BufferedReader(new StringReader(session_packet_payload));
			String session_id = session_packet_payload_reader.readLine();
			String sender_user = session_packet_payload_reader.readLine();
			String adv_sender_device = session_packet_payload_reader.readLine();
			Friend senderFriend = Memory.getFriend(sender_user);
			String encrypted_data = "";
			part = in.readLine();
			while(!part.isEmpty()) {
				encrypted_data += part + DELIM;
				part = in.readLine();
			}
			encrypted_data += DELIM;
			if(senderFriend == null) {
				SecurityMessenger.addFriend(sender_user);
				senderFriend = Memory.getFriend(sender_user);
			}
			Device senderDevice = Memory.getDevice(UUID.fromString(adv_sender_device));
			if(senderDevice == null) {
				exceptionMessage = "Your friend: " + sender_user + "is attempting to communicate with you from an unknown device. They may need to re-register their device.";
			}
			else {
				String session_key_data = CryptUtil.fromDeviceToMe(senderDevice, encrypted_data);
				System.out.println("session key data<" + session_key_data + ">");
				BufferedReader session_key_data_reader = new BufferedReader(new StringReader(session_key_data));
				String participantUsername;
				ArrayList<String> participantList = new ArrayList<>();
				participantUsername = session_key_data_reader.readLine();
				while(!participantUsername.isEmpty()) {
					participantList.add(participantUsername);
					participantUsername = session_key_data_reader.readLine();
				}
				String group_name = session_key_data_reader.readLine();
				String encoded_session_key = session_key_data_reader.readLine();
				String encoded_init_vector = session_key_data_reader.readLine();
				String expTimestamp = session_key_data_reader.readLine();
				if(participantList.contains(Memory.getUser().getUsername())) {
					System.out.println("Get group?");
					Group group = Memory.getGroup(group_name);
					if(group == null) {
						System.out.println("Create Group");
						ArrayList<Friend> friends = new ArrayList<>();
						for(String p : participantList) {
							Friend f = Memory.getFriend(p);
							if(f == null) {
								f = new Friend(p);
								SecurityMessenger.addFriend(p);
								Memory.getUser().addFriend(f);
							}
							friends.add(f);
						}
						group = new Group(friends, group_name);
						Memory.getUser().addGroup(group);
					}
					//Friend f = Memory.getFriendWithDevice(senderDevice);
					group.addSessionKey(new SessionKey(encoded_session_key, CryptUtil.decode(encoded_init_vector.getBytes(CHARSET)), expTimestamp, UUID.fromString(session_id)));
				}
				else exceptionMessage = "Received a session key that was not intended for me. Ignoring.";
			}
			if(!exceptionMessage.isEmpty()) {
				throw new PiLGramException(exceptionMessage);
			}
			return false;
		}
	}
	
	public String getSessionID() {
		return sessionID.toString();
	}

	public String getExpirationTimeStamp() {
		return expirationTimeStamp.toString();
	}
	
	public void renew(String timestamp) {
		expirationTimeStamp = renewalInterval.add(new BigInteger(timestamp));
	}
	
	public boolean isValidForMessageTimeStamp(String timestamp) {
		System.out.println("Key is valid?");
		System.out.println("For timestamp:" + timestamp);
		System.out.println("Expirationtim:" + expirationTimeStamp);
		return expirationTimeStamp.compareTo(new BigInteger(timestamp)) >= 0;
	}
	
	public SecretKey getKey() {
		return key;
	}
	
	public byte[] getIV() {
		return iv;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof SessionKey) {
			SessionKey other = (SessionKey) obj;
			return this.sessionID.toString().equals(other.getSessionID());
		}
		return false;
	}
	
}
