package client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String username;
	// Salted, hashed password
	private String shPassword;
	private UUID deviceID;
	private String publicKeyFileName;
	private String privateKeyFileName;
	private String storageFileName;
	private String lockIVFileName;
	
	private final String DFOLDER = "data/";

	private ArrayList<Group> groups = new ArrayList<>();
	private ArrayList<Friend> friends = new ArrayList<>();
	
	public User(String username, String shPassword) throws PiLGramException {
		this.username = username;
		this.shPassword = shPassword;
		this.deviceID = UUID.randomUUID();
		this.publicKeyFileName = DFOLDER + CryptUtil.filename(CryptUtil.encode(CryptUtil.hash(username + shPassword + "public"))) + ".txt";
		this.privateKeyFileName = DFOLDER + CryptUtil.filename(CryptUtil.encode(CryptUtil.hash(username + shPassword + "private"))) + ".txt";
		this.storageFileName = DFOLDER + CryptUtil.filename(CryptUtil.encode(CryptUtil.hash(username + shPassword + "storage"))) + ".txt";
		this.lockIVFileName = DFOLDER + CryptUtil.filename(CryptUtil.encode(CryptUtil.hash(username + shPassword + "lockIV"))) + ".txt";
	}
	
	public User(String username, String shPassword, UUID deviceID) throws PiLGramException {
		this.username = username;
		this.shPassword = shPassword;
		this.deviceID = deviceID;
		this.publicKeyFileName = DFOLDER + CryptUtil.filename(CryptUtil.encode(CryptUtil.hash(username + shPassword + "public"))) + ".txt";
		this.privateKeyFileName = DFOLDER + CryptUtil.filename(CryptUtil.encode(CryptUtil.hash(username + shPassword + "private"))) + ".txt";
		this.storageFileName = DFOLDER + CryptUtil.filename(CryptUtil.encode(CryptUtil.hash(username + shPassword + "storage"))) + ".txt";
		this.lockIVFileName = DFOLDER + CryptUtil.filename(CryptUtil.encode(CryptUtil.hash(username + shPassword + "lockIV"))) + ".txt";
	}

	public String getPublicKeyFileName() {
		return publicKeyFileName;
	}

	public String getPrivateKeyFileName() {
		return privateKeyFileName;
	}
	
	public String getStorageFileName() {
		return storageFileName;
	}

	public String getLockIVFileName() {
		return lockIVFileName;
	}
	
	public String getUsername() {
		return username;
	}

	public String getShPassword() {
		return shPassword;
	}

	public UUID getDeviceID() {
		return deviceID;
	}
	
	public ArrayList<Group> getGroups() {
		return groups;
	}
	
	public void addGroup(Group group) {
		groups.add(group);
	}
	
	public ArrayList<Friend> getFriends() {
		return friends;
	}
	
	public void addFriend(Friend f) {
		friends.add(f);
	}

}